/* 
 * Copyright 2015-2017 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var scope = require('./Scope');

/**
 * LexicalScope
 *
 * @returns {LexicalScope}
 */
function LexicalScope() {
    this._scope = [];
    scope.Scope.call(this);
    return this;
}

// inherit default listener
LexicalScope.prototype = Object.create(scope.Scope.prototype);
LexicalScope.prototype.constructor = LexicalScope;

/**
 * 
 * @param {type} codeblock
 * @returns {undefined}
 */
LexicalScope.prototype.push = function (codeblock) {
    this._scope.splice(0, 0, {});
    scope.Scope.prototype.push.call(this, codeblock);
};

/**
 * 
 * @returns {unresolved}
 */
LexicalScope.prototype.pop = function () {
    this._scope.splice(0, 1);
    return scope.Scope.prototype.pop.call(this);
};

/**
 * 
 * @param {type} name
 * @returns {unresolved}
 */
LexicalScope.prototype.get = function (name) {
    var stack = scope.Scope.prototype.stack.call(this);
    var p = stack[0];
    while (p !== null) {
        if (p.hasVariable(name)) {
            return p.getVariable(name);
        }
        p = p.getParent();
    }
    return scope.Scope.prototype.get.call(this, name);
};

/**
 * 
 * @param {type} name
 * @param {type} value
 * @returns {undefined}
 */
LexicalScope.prototype.set = function (name, value) {
    var stack = scope.Scope.prototype.stack.call(this);
    var p = stack[0];
    while (p !== null) {
        if (p.hasVariable(name)) {
            p.setVariable(name, value);
            break;
        }
        p = p.getParent();
    }
    if (p === null) {
        stack[0].setVariable(name, value);
    }
//        fireVariableSet(name, value);
};

/**
 * 
 * @param {type} name
 * @param {type} value
 * @returns {undefined}
 */
LexicalScope.prototype.setNew = function (name, value) {
    var stack = scope.Scope.prototype.stack.call(this);
    stack[0].setVariable(name, value);
//        fireVariableSet(name, value);
};

/**
 * 
 * @param {type} name
 * @param {type} value
 * @returns {undefined}
 */
LexicalScope.prototype.setGlobal = function (name, value) {
    scope.Scope.prototype.setGlobal.call(this, name, value);
};

/**
 * Get a function in the scope.
 * @param {type} name
 * @returns {unresolved}
 */
LexicalScope.prototype.getFunction = function (name) {
    var stack = scope.Scope.prototype.stack.call(this);
    var cb = stack[0];
    while (cb !== null) {
        if (cb.hasFunction(name)) {
            return cb.getFunction(name);
        }
        cb = cb.getParent();
    }
    return null;
};

/**
 * 
 * @returns {Object.keys|Set|nm$_LexicalScope.LexicalScope.prototype.variables.keys}
 */
LexicalScope.prototype.variables = function () {
    var stack = scope.Scope.prototype.stack.call(this);
    var keys = new Set();
    var cb = stack[0];
    while (cb !== null) {
        cb.localVariables().forEach(function (elem) {
            keys.add(elem);
        });
        cb = cb.getParent();
    }

    return keys;
};

/**
 * 
 * @param {type} value
 * @returns {unresolved}
 */
LexicalScope.prototype.variablesPeak = function (value) {
    var keys = {};
    for (var ii = this._scope.length; ii >= 0 && value >= 0; ii--, value--) {
        var slice = this._scope[ii];
        var k = Object.keys(slice);
        k.forEach(function (kelem) {
            keys[kelem] = slice[kelem];
        });
    }
    return keys;
};

exports.LexicalScope = LexicalScope;