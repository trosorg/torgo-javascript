// Generated from Logo.g4 by ANTLR 4.7
// jshint ignore: start
var antlr4 = require('antlr4/index');
var LogoListener = require('./LogoListener').LogoListener;

var grammarFileName = "Logo.g4";

var serializedATN = ["\u0003\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964",
    "\u0003T\u018d\u0004\u0002\t\u0002\u0004\u0003\t\u0003\u0004\u0004\t",
    "\u0004\u0004\u0005\t\u0005\u0004\u0006\t\u0006\u0004\u0007\t\u0007\u0004",
    "\b\t\b\u0004\t\t\t\u0004\n\t\n\u0004\u000b\t\u000b\u0004\f\t\f\u0004",
    "\r\t\r\u0004\u000e\t\u000e\u0004\u000f\t\u000f\u0004\u0010\t\u0010\u0004",
    "\u0011\t\u0011\u0004\u0012\t\u0012\u0004\u0013\t\u0013\u0004\u0014\t",
    "\u0014\u0004\u0015\t\u0015\u0004\u0016\t\u0016\u0004\u0017\t\u0017\u0004",
    "\u0018\t\u0018\u0004\u0019\t\u0019\u0004\u001a\t\u001a\u0004\u001b\t",
    "\u001b\u0004\u001c\t\u001c\u0004\u001d\t\u001d\u0004\u001e\t\u001e\u0004",
    "\u001f\t\u001f\u0004 \t \u0004!\t!\u0004\"\t\"\u0004#\t#\u0004$\t$\u0004",
    "%\t%\u0004&\t&\u0004\'\t\'\u0004(\t(\u0004)\t)\u0004*\t*\u0004+\t+\u0004",
    ",\t,\u0004-\t-\u0004.\t.\u0004/\t/\u00040\t0\u00041\t1\u00042\t2\u0004",
    "3\t3\u00044\t4\u00045\t5\u00046\t6\u0003\u0002\u0003\u0002\u0005\u0002",
    "o\n\u0002\u0003\u0002\u0006\u0002r\n\u0002\r\u0002\u000e\u0002s\u0003",
    "\u0002\u0005\u0002w\n\u0002\u0005\u0002y\n\u0002\u0003\u0003\u0006\u0003",
    "|\n\u0003\r\u0003\u000e\u0003}\u0003\u0003\u0005\u0003\u0081\n\u0003",
    "\u0003\u0003\u0003\u0003\u0003\u0003\u0005\u0003\u0086\n\u0003\u0003",
    "\u0003\u0003\u0003\u0005\u0003\u008a\n\u0003\u0003\u0004\u0003\u0004",
    "\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004",
    "\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004",
    "\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004",
    "\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004",
    "\u0005\u0004\u00a6\n\u0004\u0003\u0005\u0003\u0005\u0007\u0005\u00aa",
    "\n\u0005\f\u0005\u000e\u0005\u00ad\u000b\u0005\u0003\u0006\u0003\u0006",
    "\u0003\u0006\u0007\u0006\u00b2\n\u0006\f\u0006\u000e\u0006\u00b5\u000b",
    "\u0006\u0003\u0006\u0005\u0006\u00b8\n\u0006\u0003\u0006\u0005\u0006",
    "\u00bb\n\u0006\u0003\u0006\u0006\u0006\u00be\n\u0006\r\u0006\u000e\u0006",
    "\u00bf\u0003\u0006\u0003\u0006\u0003\u0007\u0003\u0007\u0003\u0007\u0003",
    "\u0007\u0007\u0007\u00c8\n\u0007\f\u0007\u000e\u0007\u00cb\u000b\u0007",
    "\u0003\b\u0003\b\u0003\b\u0003\b\u0003\b\u0005\b\u00d2\n\b\u0003\t\u0003",
    "\t\u0003\t\u0003\t\u0003\n\u0003\n\u0006\n\u00da\n\n\r\n\u000e\n\u00db",
    "\u0003\n\u0003\n\u0003\u000b\u0003\u000b\u0003\u000b\u0003\u000b\u0003",
    "\f\u0003\f\u0003\f\u0003\f\u0003\r\u0003\r\u0003\u000e\u0003\u000e\u0003",
    "\u000e\u0003\u000e\u0003\u000f\u0003\u000f\u0003\u000f\u0003\u000f\u0003",
    "\u0010\u0003\u0010\u0003\u0010\u0005\u0010\u00f5\n\u0010\u0003\u0011",
    "\u0003\u0011\u0003\u0011\u0007\u0011\u00fa\n\u0011\f\u0011\u000e\u0011",
    "\u00fd\u000b\u0011\u0003\u0011\u0003\u0011\u0003\u0012\u0003\u0012\u0003",
    "\u0013\u0003\u0013\u0003\u0013\u0005\u0013\u0106\n\u0013\u0003\u0014",
    "\u0003\u0014\u0003\u0014\u0003\u0014\u0003\u0015\u0005\u0015\u010d\n",
    "\u0015\u0003\u0015\u0003\u0015\u0003\u0015\u0003\u0015\u0005\u0015\u0113",
    "\n\u0015\u0003\u0016\u0003\u0016\u0003\u0016\u0005\u0016\u0118\n\u0016",
    "\u0003\u0017\u0003\u0017\u0003\u0017\u0007\u0017\u011d\n\u0017\f\u0017",
    "\u000e\u0017\u0120\u000b\u0017\u0003\u0018\u0003\u0018\u0003\u0018\u0007",
    "\u0018\u0125\n\u0018\f\u0018\u000e\u0018\u0128\u000b\u0018\u0003\u0019",
    "\u0003\u0019\u0003\u0019\u0003\u001a\u0003\u001a\u0003\u001a\u0003\u001b",
    "\u0003\u001b\u0003\u001b\u0003\u001c\u0003\u001c\u0003\u001c\u0003\u001d",
    "\u0003\u001d\u0003\u001d\u0003\u001e\u0003\u001e\u0003\u001f\u0003\u001f",
    "\u0003 \u0003 \u0003!\u0003!\u0003\"\u0003\"\u0003#\u0003#\u0003$\u0003",
    "$\u0003%\u0003%\u0003%\u0003%\u0003&\u0003&\u0003&\u0003\'\u0003\'\u0003",
    "(\u0003(\u0003)\u0003)\u0003*\u0003*\u0003+\u0003+\u0003+\u0003+\u0003",
    "+\u0003+\u0005+\u015c\n+\u0003+\u0003+\u0003+\u0003,\u0003,\u0003,\u0003",
    ",\u0003,\u0003,\u0005,\u0167\n,\u0003,\u0005,\u016a\n,\u0003-\u0003",
    "-\u0003-\u0003-\u0003-\u0003-\u0003-\u0005-\u0173\n-\u0003.\u0003.\u0003",
    ".\u0003/\u0003/\u0003/\u00030\u00030\u00030\u00031\u00031\u00031\u0003",
    "2\u00032\u00032\u00033\u00033\u00033\u00034\u00034\u00035\u00035\u0003",
    "6\u00036\u00036\u0002\u00027\u0002\u0004\u0006\b\n\f\u000e\u0010\u0012",
    "\u0014\u0016\u0018\u001a\u001c\u001e \"$&(*,.02468:<>@BDFHJLNPRTVXZ",
    "\\^`bdfhj\u0002\u0013\u0003\u0002\u000b\u0013\u0003\u0002\t\t\u0003",
    "\u0002\u0019\u001a\u0003\u0002\u001c\u001f\u0003\u0002 !\u0003\u0002",
    "\"$\u0003\u0002%&\u0003\u0002\'(\u0003\u0002),\u0003\u0002-.\u0003\u0002",
    "/0\u0003\u000212\u0003\u000234\u0003\u0002>?\u0003\u0002@A\u0003\u0002",
    "DF\u0003\u0002JM\u0002\u0198\u0002x\u0003\u0002\u0002\u0002\u0004\u0089",
    "\u0003\u0002\u0002\u0002\u0006\u00a5\u0003\u0002\u0002\u0002\b\u00a7",
    "\u0003\u0002\u0002\u0002\n\u00ae\u0003\u0002\u0002\u0002\f\u00c3\u0003",
    "\u0002\u0002\u0002\u000e\u00d1\u0003\u0002\u0002\u0002\u0010\u00d3\u0003",
    "\u0002\u0002\u0002\u0012\u00d7\u0003\u0002\u0002\u0002\u0014\u00df\u0003",
    "\u0002\u0002\u0002\u0016\u00e3\u0003\u0002\u0002\u0002\u0018\u00e7\u0003",
    "\u0002\u0002\u0002\u001a\u00e9\u0003\u0002\u0002\u0002\u001c\u00ed\u0003",
    "\u0002\u0002\u0002\u001e\u00f1\u0003\u0002\u0002\u0002 \u00f6\u0003",
    "\u0002\u0002\u0002\"\u0100\u0003\u0002\u0002\u0002$\u0105\u0003\u0002",
    "\u0002\u0002&\u0107\u0003\u0002\u0002\u0002(\u010c\u0003\u0002\u0002",
    "\u0002*\u0114\u0003\u0002\u0002\u0002,\u0119\u0003\u0002\u0002\u0002",
    ".\u0121\u0003\u0002\u0002\u00020\u0129\u0003\u0002\u0002\u00022\u012c",
    "\u0003\u0002\u0002\u00024\u012f\u0003\u0002\u0002\u00026\u0132\u0003",
    "\u0002\u0002\u00028\u0135\u0003\u0002\u0002\u0002:\u0138\u0003\u0002",
    "\u0002\u0002<\u013a\u0003\u0002\u0002\u0002>\u013c\u0003\u0002\u0002",
    "\u0002@\u013e\u0003\u0002\u0002\u0002B\u0140\u0003\u0002\u0002\u0002",
    "D\u0142\u0003\u0002\u0002\u0002F\u0144\u0003\u0002\u0002\u0002H\u0146",
    "\u0003\u0002\u0002\u0002J\u014a\u0003\u0002\u0002\u0002L\u014d\u0003",
    "\u0002\u0002\u0002N\u014f\u0003\u0002\u0002\u0002P\u0151\u0003\u0002",
    "\u0002\u0002R\u0153\u0003\u0002\u0002\u0002T\u0155\u0003\u0002\u0002",
    "\u0002V\u0160\u0003\u0002\u0002\u0002X\u016b\u0003\u0002\u0002\u0002",
    "Z\u0174\u0003\u0002\u0002\u0002\\\u0177\u0003\u0002\u0002\u0002^\u017a",
    "\u0003\u0002\u0002\u0002`\u017d\u0003\u0002\u0002\u0002b\u0180\u0003",
    "\u0002\u0002\u0002d\u0183\u0003\u0002\u0002\u0002f\u0186\u0003\u0002",
    "\u0002\u0002h\u0188\u0003\u0002\u0002\u0002j\u018a\u0003\u0002\u0002",
    "\u0002ly\u0005\u0004\u0003\u0002mo\u0005\u0004\u0003\u0002nm\u0003\u0002",
    "\u0002\u0002no\u0003\u0002\u0002\u0002op\u0003\u0002\u0002\u0002pr\u0007",
    "S\u0002\u0002qn\u0003\u0002\u0002\u0002rs\u0003\u0002\u0002\u0002sq",
    "\u0003\u0002\u0002\u0002st\u0003\u0002\u0002\u0002tv\u0003\u0002\u0002",
    "\u0002uw\u0005\u0004\u0003\u0002vu\u0003\u0002\u0002\u0002vw\u0003\u0002",
    "\u0002\u0002wy\u0003\u0002\u0002\u0002xl\u0003\u0002\u0002\u0002xq\u0003",
    "\u0002\u0002\u0002y\u0003\u0003\u0002\u0002\u0002z|\u0005\u0006\u0004",
    "\u0002{z\u0003\u0002\u0002\u0002|}\u0003\u0002\u0002\u0002}{\u0003\u0002",
    "\u0002\u0002}~\u0003\u0002\u0002\u0002~\u0080\u0003\u0002\u0002\u0002",
    "\u007f\u0081\u0005j6\u0002\u0080\u007f\u0003\u0002\u0002\u0002\u0080",
    "\u0081\u0003\u0002\u0002\u0002\u0081\u008a\u0003\u0002\u0002\u0002\u0082",
    "\u008a\u0005j6\u0002\u0083\u0085\u0005\u001e\u0010\u0002\u0084\u0086",
    "\u0005j6\u0002\u0085\u0084\u0003\u0002\u0002\u0002\u0085\u0086\u0003",
    "\u0002\u0002\u0002\u0086\u008a\u0003\u0002\u0002\u0002\u0087\u008a\u0005",
    "\n\u0006\u0002\u0088\u008a\u0007S\u0002\u0002\u0089{\u0003\u0002\u0002",
    "\u0002\u0089\u0082\u0003\u0002\u0002\u0002\u0089\u0083\u0003\u0002\u0002",
    "\u0002\u0089\u0087\u0003\u0002\u0002\u0002\u0089\u0088\u0003\u0002\u0002",
    "\u0002\u008a\u0005\u0003\u0002\u0002\u0002\u008b\u00a6\u0005\u0010\t",
    "\u0002\u008c\u00a6\u00052\u001a\u0002\u008d\u00a6\u00054\u001b\u0002",
    "\u008e\u00a6\u00056\u001c\u0002\u008f\u00a6\u00058\u001d\u0002\u0090",
    "\u00a6\u0005:\u001e\u0002\u0091\u00a6\u0005<\u001f\u0002\u0092\u00a6",
    "\u0005> \u0002\u0093\u00a6\u0005@!\u0002\u0094\u00a6\u0005B\"\u0002",
    "\u0095\u00a6\u0005D#\u0002\u0096\u00a6\u0005H%\u0002\u0097\u00a6\u0005",
    "\u001a\u000e\u0002\u0098\u00a6\u0005\u001c\u000f\u0002\u0099\u00a6\u0005",
    "\b\u0005\u0002\u009a\u00a6\u0005\u0014\u000b\u0002\u009b\u00a6\u0005",
    "F$\u0002\u009c\u00a6\u0005T+\u0002\u009d\u00a6\u0005V,\u0002\u009e\u00a6",
    "\u0005X-\u0002\u009f\u00a6\u0005\\/\u0002\u00a0\u00a6\u0005^0\u0002",
    "\u00a1\u00a6\u0005b2\u0002\u00a2\u00a6\u0005d3\u0002\u00a3\u00a6\u0005",
    "`1\u0002\u00a4\u00a6\u0005\u001e\u0010\u0002\u00a5\u008b\u0003\u0002",
    "\u0002\u0002\u00a5\u008c\u0003\u0002\u0002\u0002\u00a5\u008d\u0003\u0002",
    "\u0002\u0002\u00a5\u008e\u0003\u0002\u0002\u0002\u00a5\u008f\u0003\u0002",
    "\u0002\u0002\u00a5\u0090\u0003\u0002\u0002\u0002\u00a5\u0091\u0003\u0002",
    "\u0002\u0002\u00a5\u0092\u0003\u0002\u0002\u0002\u00a5\u0093\u0003\u0002",
    "\u0002\u0002\u00a5\u0094\u0003\u0002\u0002\u0002\u00a5\u0095\u0003\u0002",
    "\u0002\u0002\u00a5\u0096\u0003\u0002\u0002\u0002\u00a5\u0097\u0003\u0002",
    "\u0002\u0002\u00a5\u0098\u0003\u0002\u0002\u0002\u00a5\u0099\u0003\u0002",
    "\u0002\u0002\u00a5\u009a\u0003\u0002\u0002\u0002\u00a5\u009b\u0003\u0002",
    "\u0002\u0002\u00a5\u009c\u0003\u0002\u0002\u0002\u00a5\u009d\u0003\u0002",
    "\u0002\u0002\u00a5\u009e\u0003\u0002\u0002\u0002\u00a5\u009f\u0003\u0002",
    "\u0002\u0002\u00a5\u00a0\u0003\u0002\u0002\u0002\u00a5\u00a1\u0003\u0002",
    "\u0002\u0002\u00a5\u00a2\u0003\u0002\u0002\u0002\u00a5\u00a3\u0003\u0002",
    "\u0002\u0002\u00a5\u00a4\u0003\u0002\u0002\u0002\u00a6\u0007\u0003\u0002",
    "\u0002\u0002\u00a7\u00ab\u0005\"\u0012\u0002\u00a8\u00aa\u0005.\u0018",
    "\u0002\u00a9\u00a8\u0003\u0002\u0002\u0002\u00aa\u00ad\u0003\u0002\u0002",
    "\u0002\u00ab\u00a9\u0003\u0002\u0002\u0002\u00ab\u00ac\u0003\u0002\u0002",
    "\u0002\u00ac\t\u0003\u0002\u0002\u0002\u00ad\u00ab\u0003\u0002\u0002",
    "\u0002\u00ae\u00af\u0007\u0003\u0002\u0002\u00af\u00b3\u0005\"\u0012",
    "\u0002\u00b0\u00b2\u0005\f\u0007\u0002\u00b1\u00b0\u0003\u0002\u0002",
    "\u0002\u00b2\u00b5\u0003\u0002\u0002\u0002\u00b3\u00b1\u0003\u0002\u0002",
    "\u0002\u00b3\u00b4\u0003\u0002\u0002\u0002\u00b4\u00b7\u0003\u0002\u0002",
    "\u0002\u00b5\u00b3\u0003\u0002\u0002\u0002\u00b6\u00b8\u0007S\u0002",
    "\u0002\u00b7\u00b6\u0003\u0002\u0002\u0002\u00b7\u00b8\u0003\u0002\u0002",
    "\u0002\u00b8\u00bd\u0003\u0002\u0002\u0002\u00b9\u00bb\u0005\u0004\u0003",
    "\u0002\u00ba\u00b9\u0003\u0002\u0002\u0002\u00ba\u00bb\u0003\u0002\u0002",
    "\u0002\u00bb\u00bc\u0003\u0002\u0002\u0002\u00bc\u00be\u0007S\u0002",
    "\u0002\u00bd\u00ba\u0003\u0002\u0002\u0002\u00be\u00bf\u0003\u0002\u0002",
    "\u0002\u00bf\u00bd\u0003\u0002\u0002\u0002\u00bf\u00c0\u0003\u0002\u0002",
    "\u0002\u00c0\u00c1\u0003\u0002\u0002\u0002\u00c1\u00c2\u0007\u0004\u0002",
    "\u0002\u00c2\u000b\u0003\u0002\u0002\u0002\u00c3\u00c4\u0007\u0005\u0002",
    "\u0002\u00c4\u00c9\u0005\"\u0012\u0002\u00c5\u00c6\u0007\u0006\u0002",
    "\u0002\u00c6\u00c8\u0005\f\u0007\u0002\u00c7\u00c5\u0003\u0002\u0002",
    "\u0002\u00c8\u00cb\u0003\u0002\u0002\u0002\u00c9\u00c7\u0003\u0002\u0002",
    "\u0002\u00c9\u00ca\u0003\u0002\u0002\u0002\u00ca\r\u0003\u0002\u0002",
    "\u0002\u00cb\u00c9\u0003\u0002\u0002\u0002\u00cc\u00d2\u0005J&\u0002",
    "\u00cd\u00d2\u0005R*\u0002\u00ce\u00d2\u0005L\'\u0002\u00cf\u00d2\u0005",
    "N(\u0002\u00d0\u00d2\u0005P)\u0002\u00d1\u00cc\u0003\u0002\u0002\u0002",
    "\u00d1\u00cd\u0003\u0002\u0002\u0002\u00d1\u00ce\u0003\u0002\u0002\u0002",
    "\u00d1\u00cf\u0003\u0002\u0002\u0002\u00d1\u00d0\u0003\u0002\u0002\u0002",
    "\u00d2\u000f\u0003\u0002\u0002\u0002\u00d3\u00d4\u0007\u0007\u0002\u0002",
    "\u00d4\u00d5\u0005.\u0018\u0002\u00d5\u00d6\u0005\u0012\n\u0002\u00d6",
    "\u0011\u0003\u0002\u0002\u0002\u00d7\u00d9\u0007\b\u0002\u0002\u00d8",
    "\u00da\u0005\u0004\u0003\u0002\u00d9\u00d8\u0003\u0002\u0002\u0002\u00da",
    "\u00db\u0003\u0002\u0002\u0002\u00db\u00d9\u0003\u0002\u0002\u0002\u00db",
    "\u00dc\u0003\u0002\u0002\u0002\u00dc\u00dd\u0003\u0002\u0002\u0002\u00dd",
    "\u00de\u0007\t\u0002\u0002\u00de\u0013\u0003\u0002\u0002\u0002\u00df",
    "\u00e0\u0007\n\u0002\u0002\u00e0\u00e1\u0005\u0016\f\u0002\u00e1\u00e2",
    "\u0005\u0012\n\u0002\u00e2\u0015\u0003\u0002\u0002\u0002\u00e3\u00e4",
    "\u0005.\u0018\u0002\u00e4\u00e5\u0005\u0018\r\u0002\u00e5\u00e6\u0005",
    ".\u0018\u0002\u00e6\u0017\u0003\u0002\u0002\u0002\u00e7\u00e8\t\u0002",
    "\u0002\u0002\u00e8\u0019\u0003\u0002\u0002\u0002\u00e9\u00ea\u0007\u0014",
    "\u0002\u0002\u00ea\u00eb\u0007O\u0002\u0002\u00eb\u00ec\u0005$\u0013",
    "\u0002\u00ec\u001b\u0003\u0002\u0002\u0002\u00ed\u00ee\u0007\u0015\u0002",
    "\u0002\u00ee\u00ef\u0007O\u0002\u0002\u00ef\u00f0\u0005$\u0013\u0002",
    "\u00f0\u001d\u0003\u0002\u0002\u0002\u00f1\u00f4\u0007\u0016\u0002\u0002",
    "\u00f2\u00f5\u0005$\u0013\u0002\u00f3\u00f5\u0005 \u0011\u0002\u00f4",
    "\u00f2\u0003\u0002\u0002\u0002\u00f4\u00f3\u0003\u0002\u0002\u0002\u00f5",
    "\u001f\u0003\u0002\u0002\u0002\u00f6\u00fb\u0007\b\u0002\u0002\u00f7",
    "\u00fa\u0005 \u0011\u0002\u00f8\u00fa\n\u0003\u0002\u0002\u00f9\u00f7",
    "\u0003\u0002\u0002\u0002\u00f9\u00f8\u0003\u0002\u0002\u0002\u00fa\u00fd",
    "\u0003\u0002\u0002\u0002\u00fb\u00f9\u0003\u0002\u0002\u0002\u00fb\u00fc",
    "\u0003\u0002\u0002\u0002\u00fc\u00fe\u0003\u0002\u0002\u0002\u00fd\u00fb",
    "\u0003\u0002\u0002\u0002\u00fe\u00ff\u0007\t\u0002\u0002\u00ff!\u0003",
    "\u0002\u0002\u0002\u0100\u0101\u0007P\u0002\u0002\u0101#\u0003\u0002",
    "\u0002\u0002\u0102\u0106\u0007O\u0002\u0002\u0103\u0106\u0005.\u0018",
    "\u0002\u0104\u0106\u00050\u0019\u0002\u0105\u0102\u0003\u0002\u0002",
    "\u0002\u0105\u0103\u0003\u0002\u0002\u0002\u0105\u0104\u0003\u0002\u0002",
    "\u0002\u0106%\u0003\u0002\u0002\u0002\u0107\u0108\u0007\u0017\u0002",
    "\u0002\u0108\u0109\u0005.\u0018\u0002\u0109\u010a\u0007\u0018\u0002",
    "\u0002\u010a\'\u0003\u0002\u0002\u0002\u010b\u010d\t\u0004\u0002\u0002",
    "\u010c\u010b\u0003\u0002\u0002\u0002\u010c\u010d\u0003\u0002\u0002\u0002",
    "\u010d\u0112\u0003\u0002\u0002\u0002\u010e\u0113\u0005h5\u0002\u010f",
    "\u0113\u00050\u0019\u0002\u0110\u0113\u0005\u000e\b\u0002\u0111\u0113",
    "\u0005&\u0014\u0002\u0112\u010e\u0003\u0002\u0002\u0002\u0112\u010f",
    "\u0003\u0002\u0002\u0002\u0112\u0110\u0003\u0002\u0002\u0002\u0112\u0111",
    "\u0003\u0002\u0002\u0002\u0113)\u0003\u0002\u0002\u0002\u0114\u0117",
    "\u0005(\u0015\u0002\u0115\u0116\u0007\u001b\u0002\u0002\u0116\u0118",
    "\u0005(\u0015\u0002\u0117\u0115\u0003\u0002\u0002\u0002\u0117\u0118",
    "\u0003\u0002\u0002\u0002\u0118+\u0003\u0002\u0002\u0002\u0119\u011e",
    "\u0005*\u0016\u0002\u011a\u011b\t\u0005\u0002\u0002\u011b\u011d\u0005",
    "*\u0016\u0002\u011c\u011a\u0003\u0002\u0002\u0002\u011d\u0120\u0003",
    "\u0002\u0002\u0002\u011e\u011c\u0003\u0002\u0002\u0002\u011e\u011f\u0003",
    "\u0002\u0002\u0002\u011f-\u0003\u0002\u0002\u0002\u0120\u011e\u0003",
    "\u0002\u0002\u0002\u0121\u0126\u0005,\u0017\u0002\u0122\u0123\t\u0004",
    "\u0002\u0002\u0123\u0125\u0005,\u0017\u0002\u0124\u0122\u0003\u0002",
    "\u0002\u0002\u0125\u0128\u0003\u0002\u0002\u0002\u0126\u0124\u0003\u0002",
    "\u0002\u0002\u0126\u0127\u0003\u0002\u0002\u0002\u0127/\u0003\u0002",
    "\u0002\u0002\u0128\u0126\u0003\u0002\u0002\u0002\u0129\u012a\u0007\u0005",
    "\u0002\u0002\u012a\u012b\u0005\"\u0012\u0002\u012b1\u0003\u0002\u0002",
    "\u0002\u012c\u012d\t\u0006\u0002\u0002\u012d\u012e\u0005.\u0018\u0002",
    "\u012e3\u0003\u0002\u0002\u0002\u012f\u0130\t\u0007\u0002\u0002\u0130",
    "\u0131\u0005.\u0018\u0002\u01315\u0003\u0002\u0002\u0002\u0132\u0133",
    "\t\b\u0002\u0002\u0133\u0134\u0005.\u0018\u0002\u01347\u0003\u0002\u0002",
    "\u0002\u0135\u0136\t\t\u0002\u0002\u0136\u0137\u0005.\u0018\u0002\u0137",
    "9\u0003\u0002\u0002\u0002\u0138\u0139\t\n\u0002\u0002\u0139;\u0003\u0002",
    "\u0002\u0002\u013a\u013b\t\u000b\u0002\u0002\u013b=\u0003\u0002\u0002",
    "\u0002\u013c\u013d\t\f\u0002\u0002\u013d?\u0003\u0002\u0002\u0002\u013e",
    "\u013f\t\r\u0002\u0002\u013fA\u0003\u0002\u0002\u0002\u0140\u0141\t",
    "\u000e\u0002\u0002\u0141C\u0003\u0002\u0002\u0002\u0142\u0143\u0007",
    "5\u0002\u0002\u0143E\u0003\u0002\u0002\u0002\u0144\u0145\u00076\u0002",
    "\u0002\u0145G\u0003\u0002\u0002\u0002\u0146\u0147\u00077\u0002\u0002",
    "\u0147\u0148\u0005.\u0018\u0002\u0148\u0149\u0005.\u0018\u0002\u0149",
    "I\u0003\u0002\u0002\u0002\u014a\u014b\u00078\u0002\u0002\u014b\u014c",
    "\u0005.\u0018\u0002\u014cK\u0003\u0002\u0002\u0002\u014d\u014e\u0007",
    "9\u0002\u0002\u014eM\u0003\u0002\u0002\u0002\u014f\u0150\u0007:\u0002",
    "\u0002\u0150O\u0003\u0002\u0002\u0002\u0151\u0152\u0007;\u0002\u0002",
    "\u0152Q\u0003\u0002\u0002\u0002\u0153\u0154\u0007<\u0002\u0002\u0154",
    "S\u0003\u0002\u0002\u0002\u0155\u0156\u0007=\u0002\u0002\u0156\u0157",
    "\u0007\b\u0002\u0002\u0157\u0158\u0005\"\u0012\u0002\u0158\u0159\u0005",
    ".\u0018\u0002\u0159\u015b\u0005.\u0018\u0002\u015a\u015c\u0005.\u0018",
    "\u0002\u015b\u015a\u0003\u0002\u0002\u0002\u015b\u015c\u0003\u0002\u0002",
    "\u0002\u015c\u015d\u0003\u0002\u0002\u0002\u015d\u015e\u0007\t\u0002",
    "\u0002\u015e\u015f\u0005\u0012\n\u0002\u015fU\u0003\u0002\u0002\u0002",
    "\u0160\u0169\t\u000f\u0002\u0002\u0161\u016a\u0005\"\u0012\u0002\u0162",
    "\u0163\u0005.\u0018\u0002\u0163\u0164\u0005.\u0018\u0002\u0164\u0166",
    "\u0005.\u0018\u0002\u0165\u0167\u0005.\u0018\u0002\u0166\u0165\u0003",
    "\u0002\u0002\u0002\u0166\u0167\u0003\u0002\u0002\u0002\u0167\u016a\u0003",
    "\u0002\u0002\u0002\u0168\u016a\u0005Z.\u0002\u0169\u0161\u0003\u0002",
    "\u0002\u0002\u0169\u0162\u0003\u0002\u0002\u0002\u0169\u0168\u0003\u0002",
    "\u0002\u0002\u016aW\u0003\u0002\u0002\u0002\u016b\u0172\t\u0010\u0002",
    "\u0002\u016c\u0173\u0005\"\u0012\u0002\u016d\u016e\u0005.\u0018\u0002",
    "\u016e\u016f\u0005.\u0018\u0002\u016f\u0170\u0005.\u0018\u0002\u0170",
    "\u0173\u0003\u0002\u0002\u0002\u0171\u0173\u0005Z.\u0002\u0172\u016c",
    "\u0003\u0002\u0002\u0002\u0172\u016d\u0003\u0002\u0002\u0002\u0172\u0171",
    "\u0003\u0002\u0002\u0002\u0173Y\u0003\u0002\u0002\u0002\u0174\u0175",
    "\u0007B\u0002\u0002\u0175\u0176\u0007N\u0002\u0002\u0176[\u0003\u0002",
    "\u0002\u0002\u0177\u0178\u0007C\u0002\u0002\u0178\u0179\u0005.\u0018",
    "\u0002\u0179]\u0003\u0002\u0002\u0002\u017a\u017b\t\u0011\u0002\u0002",
    "\u017b\u017c\u0005$\u0013\u0002\u017c_\u0003\u0002\u0002\u0002\u017d",
    "\u017e\u0007G\u0002\u0002\u017e\u017f\u0005\"\u0012\u0002\u017fa\u0003",
    "\u0002\u0002\u0002\u0180\u0181\u0007H\u0002\u0002\u0181\u0182\u0005",
    ".\u0018\u0002\u0182c\u0003\u0002\u0002\u0002\u0183\u0184\u0007I\u0002",
    "\u0002\u0184\u0185\u0005f4\u0002\u0185e\u0003\u0002\u0002\u0002\u0186",
    "\u0187\t\u0012\u0002\u0002\u0187g\u0003\u0002\u0002\u0002\u0188\u0189",
    "\u0007Q\u0002\u0002\u0189i\u0003\u0002\u0002\u0002\u018a\u018b\u0007",
    "R\u0002\u0002\u018bk\u0003\u0002\u0002\u0002 nsvx}\u0080\u0085\u0089",
    "\u00a5\u00ab\u00b3\u00b7\u00ba\u00bf\u00c9\u00d1\u00db\u00f4\u00f9\u00fb",
    "\u0105\u010c\u0112\u0117\u011e\u0126\u015b\u0166\u0169\u0172"].join("");


var atn = new antlr4.atn.ATNDeserializer().deserialize(serializedATN);

var decisionsToDFA = atn.decisionToState.map( function(ds, index) { return new antlr4.dfa.DFA(ds, index); });

var sharedContextCache = new antlr4.PredictionContextCache();

var literalNames = [ null, "'to'", "'end'", "':'", "','", "'repeat'", "'['", 
                     "']'", "'if'", "'<'", "'>'", "'='", "'!'", "'<='", 
                     "'>='", "'=='", "'!='", "'<>'", "'make'", "'localmake'", 
                     "'print'", "'('", "')'", "'+'", "'-'", "'^'", "'*'", 
                     "'/'", "'\\'", "'%'", "'fd'", "'forward'", "'bk'", 
                     "'backward'", "'back'", "'rt'", "'right'", "'lt'", 
                     "'left'", "'cs'", "'clearscreen'", "'cls'", "'clear'", 
                     "'pu'", "'penup'", "'pd'", "'pendown'", "'ht'", "'hideturtle'", 
                     "'st'", "'showturtle'", "'home'", "'stop'", "'setxy'", 
                     "'random'", "'getangle'", "'getx'", "'gety'", "'repcount'", 
                     "'for'", "'pc'", "'pencolor'", "'cc'", "'canvascolor'", 
                     "'#'", "'pause'", "'ds'", "'drawstring'", "'label'", 
                     "'fontname'", "'fontsize'", "'fontstyle'", "'bold'", 
                     "'plain'", "'italic'", "'bold_italic'" ];

var symbolicNames = [ null, null, null, null, null, null, null, null, null, 
                      null, null, null, null, null, null, null, null, null, 
                      null, null, null, null, null, null, null, null, null, 
                      null, null, null, null, null, null, null, null, null, 
                      null, null, null, null, null, null, null, null, null, 
                      null, null, null, null, null, null, null, null, null, 
                      null, null, null, null, null, null, null, null, null, 
                      null, null, null, null, null, null, null, null, null, 
                      null, null, null, null, "HEX", "STRINGLITERAL", "STRING", 
                      "NUMBER", "COMMENT", "EOL", "WS" ];

var ruleNames =  [ "prog", "line", "cmd", "procedureInvocation", "procedureDeclaration", 
                   "parameterDeclarations", "func", "repeat", "block", "ife", 
                   "comparison", "comparisonOperator", "make", "localmake", 
                   "print_command", "quotedstring", "name", "value", "parenExpression", 
                   "signExpression", "powerExpression", "multiplyingExpression", 
                   "expression", "deref", "fd", "bk", "rt", "lt", "cs", 
                   "pu", "pd", "ht", "st", "home", "stop", "setxy", "random", 
                   "getangle", "getx", "gety", "repcount", "fore", "pc", 
                   "cc", "hexcolor", "pause", "ds", "fontname", "fontsize", 
                   "fontstyle", "style", "number", "comment" ];

function LogoParser (input) {
	antlr4.Parser.call(this, input);
    this._interp = new antlr4.atn.ParserATNSimulator(this, atn, decisionsToDFA, sharedContextCache);
    this.ruleNames = ruleNames;
    this.literalNames = literalNames;
    this.symbolicNames = symbolicNames;
    return this;
}

LogoParser.prototype = Object.create(antlr4.Parser.prototype);
LogoParser.prototype.constructor = LogoParser;

Object.defineProperty(LogoParser.prototype, "atn", {
	get : function() {
		return atn;
	}
});

LogoParser.EOF = antlr4.Token.EOF;
LogoParser.T__0 = 1;
LogoParser.T__1 = 2;
LogoParser.T__2 = 3;
LogoParser.T__3 = 4;
LogoParser.T__4 = 5;
LogoParser.T__5 = 6;
LogoParser.T__6 = 7;
LogoParser.T__7 = 8;
LogoParser.T__8 = 9;
LogoParser.T__9 = 10;
LogoParser.T__10 = 11;
LogoParser.T__11 = 12;
LogoParser.T__12 = 13;
LogoParser.T__13 = 14;
LogoParser.T__14 = 15;
LogoParser.T__15 = 16;
LogoParser.T__16 = 17;
LogoParser.T__17 = 18;
LogoParser.T__18 = 19;
LogoParser.T__19 = 20;
LogoParser.T__20 = 21;
LogoParser.T__21 = 22;
LogoParser.T__22 = 23;
LogoParser.T__23 = 24;
LogoParser.T__24 = 25;
LogoParser.T__25 = 26;
LogoParser.T__26 = 27;
LogoParser.T__27 = 28;
LogoParser.T__28 = 29;
LogoParser.T__29 = 30;
LogoParser.T__30 = 31;
LogoParser.T__31 = 32;
LogoParser.T__32 = 33;
LogoParser.T__33 = 34;
LogoParser.T__34 = 35;
LogoParser.T__35 = 36;
LogoParser.T__36 = 37;
LogoParser.T__37 = 38;
LogoParser.T__38 = 39;
LogoParser.T__39 = 40;
LogoParser.T__40 = 41;
LogoParser.T__41 = 42;
LogoParser.T__42 = 43;
LogoParser.T__43 = 44;
LogoParser.T__44 = 45;
LogoParser.T__45 = 46;
LogoParser.T__46 = 47;
LogoParser.T__47 = 48;
LogoParser.T__48 = 49;
LogoParser.T__49 = 50;
LogoParser.T__50 = 51;
LogoParser.T__51 = 52;
LogoParser.T__52 = 53;
LogoParser.T__53 = 54;
LogoParser.T__54 = 55;
LogoParser.T__55 = 56;
LogoParser.T__56 = 57;
LogoParser.T__57 = 58;
LogoParser.T__58 = 59;
LogoParser.T__59 = 60;
LogoParser.T__60 = 61;
LogoParser.T__61 = 62;
LogoParser.T__62 = 63;
LogoParser.T__63 = 64;
LogoParser.T__64 = 65;
LogoParser.T__65 = 66;
LogoParser.T__66 = 67;
LogoParser.T__67 = 68;
LogoParser.T__68 = 69;
LogoParser.T__69 = 70;
LogoParser.T__70 = 71;
LogoParser.T__71 = 72;
LogoParser.T__72 = 73;
LogoParser.T__73 = 74;
LogoParser.T__74 = 75;
LogoParser.HEX = 76;
LogoParser.STRINGLITERAL = 77;
LogoParser.STRING = 78;
LogoParser.NUMBER = 79;
LogoParser.COMMENT = 80;
LogoParser.EOL = 81;
LogoParser.WS = 82;

LogoParser.RULE_prog = 0;
LogoParser.RULE_line = 1;
LogoParser.RULE_cmd = 2;
LogoParser.RULE_procedureInvocation = 3;
LogoParser.RULE_procedureDeclaration = 4;
LogoParser.RULE_parameterDeclarations = 5;
LogoParser.RULE_func = 6;
LogoParser.RULE_repeat = 7;
LogoParser.RULE_block = 8;
LogoParser.RULE_ife = 9;
LogoParser.RULE_comparison = 10;
LogoParser.RULE_comparisonOperator = 11;
LogoParser.RULE_make = 12;
LogoParser.RULE_localmake = 13;
LogoParser.RULE_print_command = 14;
LogoParser.RULE_quotedstring = 15;
LogoParser.RULE_name = 16;
LogoParser.RULE_value = 17;
LogoParser.RULE_parenExpression = 18;
LogoParser.RULE_signExpression = 19;
LogoParser.RULE_powerExpression = 20;
LogoParser.RULE_multiplyingExpression = 21;
LogoParser.RULE_expression = 22;
LogoParser.RULE_deref = 23;
LogoParser.RULE_fd = 24;
LogoParser.RULE_bk = 25;
LogoParser.RULE_rt = 26;
LogoParser.RULE_lt = 27;
LogoParser.RULE_cs = 28;
LogoParser.RULE_pu = 29;
LogoParser.RULE_pd = 30;
LogoParser.RULE_ht = 31;
LogoParser.RULE_st = 32;
LogoParser.RULE_home = 33;
LogoParser.RULE_stop = 34;
LogoParser.RULE_setxy = 35;
LogoParser.RULE_random = 36;
LogoParser.RULE_getangle = 37;
LogoParser.RULE_getx = 38;
LogoParser.RULE_gety = 39;
LogoParser.RULE_repcount = 40;
LogoParser.RULE_fore = 41;
LogoParser.RULE_pc = 42;
LogoParser.RULE_cc = 43;
LogoParser.RULE_hexcolor = 44;
LogoParser.RULE_pause = 45;
LogoParser.RULE_ds = 46;
LogoParser.RULE_fontname = 47;
LogoParser.RULE_fontsize = 48;
LogoParser.RULE_fontstyle = 49;
LogoParser.RULE_style = 50;
LogoParser.RULE_number = 51;
LogoParser.RULE_comment = 52;

function ProgContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_prog;
    return this;
}

ProgContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ProgContext.prototype.constructor = ProgContext;

ProgContext.prototype.line = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(LineContext);
    } else {
        return this.getTypedRuleContext(LineContext,i);
    }
};

ProgContext.prototype.EOL = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(LogoParser.EOL);
    } else {
        return this.getToken(LogoParser.EOL, i);
    }
};


ProgContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterProg(this);
	}
};

ProgContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitProg(this);
	}
};




LogoParser.ProgContext = ProgContext;

LogoParser.prototype.prog = function() {

    var localctx = new ProgContext(this, this._ctx, this.state);
    this.enterRule(localctx, 0, LogoParser.RULE_prog);
    var _la = 0; // Token type
    try {
        this.state = 118;
        this._errHandler.sync(this);
        var la_ = this._interp.adaptivePredict(this._input,3,this._ctx);
        switch(la_) {
        case 1:
            this.enterOuterAlt(localctx, 1);
            this.state = 106;
            this.line();
            break;

        case 2:
            this.enterOuterAlt(localctx, 2);
            this.state = 111; 
            this._errHandler.sync(this);
            var _alt = 1;
            do {
            	switch (_alt) {
            	case 1:
            		this.state = 108;
            		this._errHandler.sync(this);
            		var la_ = this._interp.adaptivePredict(this._input,0,this._ctx);
            		if(la_===1) {
            		    this.state = 107;
            		    this.line();

            		}
            		this.state = 110;
            		this.match(LogoParser.EOL);
            		break;
            	default:
            		throw new antlr4.error.NoViableAltException(this);
            	}
            	this.state = 113; 
            	this._errHandler.sync(this);
            	_alt = this._interp.adaptivePredict(this._input,1, this._ctx);
            } while ( _alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER );
            this.state = 116;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
            if((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << LogoParser.T__0) | (1 << LogoParser.T__4) | (1 << LogoParser.T__7) | (1 << LogoParser.T__17) | (1 << LogoParser.T__18) | (1 << LogoParser.T__19) | (1 << LogoParser.T__29) | (1 << LogoParser.T__30))) !== 0) || ((((_la - 32)) & ~0x1f) == 0 && ((1 << (_la - 32)) & ((1 << (LogoParser.T__31 - 32)) | (1 << (LogoParser.T__32 - 32)) | (1 << (LogoParser.T__33 - 32)) | (1 << (LogoParser.T__34 - 32)) | (1 << (LogoParser.T__35 - 32)) | (1 << (LogoParser.T__36 - 32)) | (1 << (LogoParser.T__37 - 32)) | (1 << (LogoParser.T__38 - 32)) | (1 << (LogoParser.T__39 - 32)) | (1 << (LogoParser.T__40 - 32)) | (1 << (LogoParser.T__41 - 32)) | (1 << (LogoParser.T__42 - 32)) | (1 << (LogoParser.T__43 - 32)) | (1 << (LogoParser.T__44 - 32)) | (1 << (LogoParser.T__45 - 32)) | (1 << (LogoParser.T__46 - 32)) | (1 << (LogoParser.T__47 - 32)) | (1 << (LogoParser.T__48 - 32)) | (1 << (LogoParser.T__49 - 32)) | (1 << (LogoParser.T__50 - 32)) | (1 << (LogoParser.T__51 - 32)) | (1 << (LogoParser.T__52 - 32)) | (1 << (LogoParser.T__58 - 32)) | (1 << (LogoParser.T__59 - 32)) | (1 << (LogoParser.T__60 - 32)) | (1 << (LogoParser.T__61 - 32)) | (1 << (LogoParser.T__62 - 32)))) !== 0) || ((((_la - 65)) & ~0x1f) == 0 && ((1 << (_la - 65)) & ((1 << (LogoParser.T__64 - 65)) | (1 << (LogoParser.T__65 - 65)) | (1 << (LogoParser.T__66 - 65)) | (1 << (LogoParser.T__67 - 65)) | (1 << (LogoParser.T__68 - 65)) | (1 << (LogoParser.T__69 - 65)) | (1 << (LogoParser.T__70 - 65)) | (1 << (LogoParser.STRING - 65)) | (1 << (LogoParser.COMMENT - 65)) | (1 << (LogoParser.EOL - 65)))) !== 0)) {
                this.state = 115;
                this.line();
            }

            break;

        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function LineContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_line;
    return this;
}

LineContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
LineContext.prototype.constructor = LineContext;

LineContext.prototype.cmd = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(CmdContext);
    } else {
        return this.getTypedRuleContext(CmdContext,i);
    }
};

LineContext.prototype.comment = function() {
    return this.getTypedRuleContext(CommentContext,0);
};

LineContext.prototype.print_command = function() {
    return this.getTypedRuleContext(Print_commandContext,0);
};

LineContext.prototype.procedureDeclaration = function() {
    return this.getTypedRuleContext(ProcedureDeclarationContext,0);
};

LineContext.prototype.EOL = function() {
    return this.getToken(LogoParser.EOL, 0);
};

LineContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterLine(this);
	}
};

LineContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitLine(this);
	}
};




LogoParser.LineContext = LineContext;

LogoParser.prototype.line = function() {

    var localctx = new LineContext(this, this._ctx, this.state);
    this.enterRule(localctx, 2, LogoParser.RULE_line);
    try {
        this.state = 135;
        this._errHandler.sync(this);
        var la_ = this._interp.adaptivePredict(this._input,7,this._ctx);
        switch(la_) {
        case 1:
            this.enterOuterAlt(localctx, 1);
            this.state = 121; 
            this._errHandler.sync(this);
            var _alt = 1;
            do {
            	switch (_alt) {
            	case 1:
            		this.state = 120;
            		this.cmd();
            		break;
            	default:
            		throw new antlr4.error.NoViableAltException(this);
            	}
            	this.state = 123; 
            	this._errHandler.sync(this);
            	_alt = this._interp.adaptivePredict(this._input,4, this._ctx);
            } while ( _alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER );
            this.state = 126;
            this._errHandler.sync(this);
            var la_ = this._interp.adaptivePredict(this._input,5,this._ctx);
            if(la_===1) {
                this.state = 125;
                this.comment();

            }
            break;

        case 2:
            this.enterOuterAlt(localctx, 2);
            this.state = 128;
            this.comment();
            break;

        case 3:
            this.enterOuterAlt(localctx, 3);
            this.state = 129;
            this.print_command();
            this.state = 131;
            this._errHandler.sync(this);
            var la_ = this._interp.adaptivePredict(this._input,6,this._ctx);
            if(la_===1) {
                this.state = 130;
                this.comment();

            }
            break;

        case 4:
            this.enterOuterAlt(localctx, 4);
            this.state = 133;
            this.procedureDeclaration();
            break;

        case 5:
            this.enterOuterAlt(localctx, 5);
            this.state = 134;
            this.match(LogoParser.EOL);
            break;

        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function CmdContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_cmd;
    return this;
}

CmdContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
CmdContext.prototype.constructor = CmdContext;

CmdContext.prototype.repeat = function() {
    return this.getTypedRuleContext(RepeatContext,0);
};

CmdContext.prototype.fd = function() {
    return this.getTypedRuleContext(FdContext,0);
};

CmdContext.prototype.bk = function() {
    return this.getTypedRuleContext(BkContext,0);
};

CmdContext.prototype.rt = function() {
    return this.getTypedRuleContext(RtContext,0);
};

CmdContext.prototype.lt = function() {
    return this.getTypedRuleContext(LtContext,0);
};

CmdContext.prototype.cs = function() {
    return this.getTypedRuleContext(CsContext,0);
};

CmdContext.prototype.pu = function() {
    return this.getTypedRuleContext(PuContext,0);
};

CmdContext.prototype.pd = function() {
    return this.getTypedRuleContext(PdContext,0);
};

CmdContext.prototype.ht = function() {
    return this.getTypedRuleContext(HtContext,0);
};

CmdContext.prototype.st = function() {
    return this.getTypedRuleContext(StContext,0);
};

CmdContext.prototype.home = function() {
    return this.getTypedRuleContext(HomeContext,0);
};

CmdContext.prototype.setxy = function() {
    return this.getTypedRuleContext(SetxyContext,0);
};

CmdContext.prototype.make = function() {
    return this.getTypedRuleContext(MakeContext,0);
};

CmdContext.prototype.localmake = function() {
    return this.getTypedRuleContext(LocalmakeContext,0);
};

CmdContext.prototype.procedureInvocation = function() {
    return this.getTypedRuleContext(ProcedureInvocationContext,0);
};

CmdContext.prototype.ife = function() {
    return this.getTypedRuleContext(IfeContext,0);
};

CmdContext.prototype.stop = function() {
    return this.getTypedRuleContext(StopContext,0);
};

CmdContext.prototype.fore = function() {
    return this.getTypedRuleContext(ForeContext,0);
};

CmdContext.prototype.pc = function() {
    return this.getTypedRuleContext(PcContext,0);
};

CmdContext.prototype.cc = function() {
    return this.getTypedRuleContext(CcContext,0);
};

CmdContext.prototype.pause = function() {
    return this.getTypedRuleContext(PauseContext,0);
};

CmdContext.prototype.ds = function() {
    return this.getTypedRuleContext(DsContext,0);
};

CmdContext.prototype.fontsize = function() {
    return this.getTypedRuleContext(FontsizeContext,0);
};

CmdContext.prototype.fontstyle = function() {
    return this.getTypedRuleContext(FontstyleContext,0);
};

CmdContext.prototype.fontname = function() {
    return this.getTypedRuleContext(FontnameContext,0);
};

CmdContext.prototype.print_command = function() {
    return this.getTypedRuleContext(Print_commandContext,0);
};

CmdContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterCmd(this);
	}
};

CmdContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitCmd(this);
	}
};




LogoParser.CmdContext = CmdContext;

LogoParser.prototype.cmd = function() {

    var localctx = new CmdContext(this, this._ctx, this.state);
    this.enterRule(localctx, 4, LogoParser.RULE_cmd);
    try {
        this.state = 163;
        this._errHandler.sync(this);
        switch(this._input.LA(1)) {
        case LogoParser.T__4:
            this.enterOuterAlt(localctx, 1);
            this.state = 137;
            this.repeat();
            break;
        case LogoParser.T__29:
        case LogoParser.T__30:
            this.enterOuterAlt(localctx, 2);
            this.state = 138;
            this.fd();
            break;
        case LogoParser.T__31:
        case LogoParser.T__32:
        case LogoParser.T__33:
            this.enterOuterAlt(localctx, 3);
            this.state = 139;
            this.bk();
            break;
        case LogoParser.T__34:
        case LogoParser.T__35:
            this.enterOuterAlt(localctx, 4);
            this.state = 140;
            this.rt();
            break;
        case LogoParser.T__36:
        case LogoParser.T__37:
            this.enterOuterAlt(localctx, 5);
            this.state = 141;
            this.lt();
            break;
        case LogoParser.T__38:
        case LogoParser.T__39:
        case LogoParser.T__40:
        case LogoParser.T__41:
            this.enterOuterAlt(localctx, 6);
            this.state = 142;
            this.cs();
            break;
        case LogoParser.T__42:
        case LogoParser.T__43:
            this.enterOuterAlt(localctx, 7);
            this.state = 143;
            this.pu();
            break;
        case LogoParser.T__44:
        case LogoParser.T__45:
            this.enterOuterAlt(localctx, 8);
            this.state = 144;
            this.pd();
            break;
        case LogoParser.T__46:
        case LogoParser.T__47:
            this.enterOuterAlt(localctx, 9);
            this.state = 145;
            this.ht();
            break;
        case LogoParser.T__48:
        case LogoParser.T__49:
            this.enterOuterAlt(localctx, 10);
            this.state = 146;
            this.st();
            break;
        case LogoParser.T__50:
            this.enterOuterAlt(localctx, 11);
            this.state = 147;
            this.home();
            break;
        case LogoParser.T__52:
            this.enterOuterAlt(localctx, 12);
            this.state = 148;
            this.setxy();
            break;
        case LogoParser.T__17:
            this.enterOuterAlt(localctx, 13);
            this.state = 149;
            this.make();
            break;
        case LogoParser.T__18:
            this.enterOuterAlt(localctx, 14);
            this.state = 150;
            this.localmake();
            break;
        case LogoParser.STRING:
            this.enterOuterAlt(localctx, 15);
            this.state = 151;
            this.procedureInvocation();
            break;
        case LogoParser.T__7:
            this.enterOuterAlt(localctx, 16);
            this.state = 152;
            this.ife();
            break;
        case LogoParser.T__51:
            this.enterOuterAlt(localctx, 17);
            this.state = 153;
            this.stop();
            break;
        case LogoParser.T__58:
            this.enterOuterAlt(localctx, 18);
            this.state = 154;
            this.fore();
            break;
        case LogoParser.T__59:
        case LogoParser.T__60:
            this.enterOuterAlt(localctx, 19);
            this.state = 155;
            this.pc();
            break;
        case LogoParser.T__61:
        case LogoParser.T__62:
            this.enterOuterAlt(localctx, 20);
            this.state = 156;
            this.cc();
            break;
        case LogoParser.T__64:
            this.enterOuterAlt(localctx, 21);
            this.state = 157;
            this.pause();
            break;
        case LogoParser.T__65:
        case LogoParser.T__66:
        case LogoParser.T__67:
            this.enterOuterAlt(localctx, 22);
            this.state = 158;
            this.ds();
            break;
        case LogoParser.T__69:
            this.enterOuterAlt(localctx, 23);
            this.state = 159;
            this.fontsize();
            break;
        case LogoParser.T__70:
            this.enterOuterAlt(localctx, 24);
            this.state = 160;
            this.fontstyle();
            break;
        case LogoParser.T__68:
            this.enterOuterAlt(localctx, 25);
            this.state = 161;
            this.fontname();
            break;
        case LogoParser.T__19:
            this.enterOuterAlt(localctx, 26);
            this.state = 162;
            this.print_command();
            break;
        default:
            throw new antlr4.error.NoViableAltException(this);
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ProcedureInvocationContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_procedureInvocation;
    return this;
}

ProcedureInvocationContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ProcedureInvocationContext.prototype.constructor = ProcedureInvocationContext;

ProcedureInvocationContext.prototype.name = function() {
    return this.getTypedRuleContext(NameContext,0);
};

ProcedureInvocationContext.prototype.expression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ExpressionContext);
    } else {
        return this.getTypedRuleContext(ExpressionContext,i);
    }
};

ProcedureInvocationContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterProcedureInvocation(this);
	}
};

ProcedureInvocationContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitProcedureInvocation(this);
	}
};




LogoParser.ProcedureInvocationContext = ProcedureInvocationContext;

LogoParser.prototype.procedureInvocation = function() {

    var localctx = new ProcedureInvocationContext(this, this._ctx, this.state);
    this.enterRule(localctx, 6, LogoParser.RULE_procedureInvocation);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 165;
        this.name();
        this.state = 169;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << LogoParser.T__2) | (1 << LogoParser.T__20) | (1 << LogoParser.T__22) | (1 << LogoParser.T__23))) !== 0) || ((((_la - 54)) & ~0x1f) == 0 && ((1 << (_la - 54)) & ((1 << (LogoParser.T__53 - 54)) | (1 << (LogoParser.T__54 - 54)) | (1 << (LogoParser.T__55 - 54)) | (1 << (LogoParser.T__56 - 54)) | (1 << (LogoParser.T__57 - 54)) | (1 << (LogoParser.NUMBER - 54)))) !== 0)) {
            this.state = 166;
            this.expression();
            this.state = 171;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ProcedureDeclarationContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_procedureDeclaration;
    return this;
}

ProcedureDeclarationContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ProcedureDeclarationContext.prototype.constructor = ProcedureDeclarationContext;

ProcedureDeclarationContext.prototype.name = function() {
    return this.getTypedRuleContext(NameContext,0);
};

ProcedureDeclarationContext.prototype.parameterDeclarations = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ParameterDeclarationsContext);
    } else {
        return this.getTypedRuleContext(ParameterDeclarationsContext,i);
    }
};

ProcedureDeclarationContext.prototype.EOL = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(LogoParser.EOL);
    } else {
        return this.getToken(LogoParser.EOL, i);
    }
};


ProcedureDeclarationContext.prototype.line = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(LineContext);
    } else {
        return this.getTypedRuleContext(LineContext,i);
    }
};

ProcedureDeclarationContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterProcedureDeclaration(this);
	}
};

ProcedureDeclarationContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitProcedureDeclaration(this);
	}
};




LogoParser.ProcedureDeclarationContext = ProcedureDeclarationContext;

LogoParser.prototype.procedureDeclaration = function() {

    var localctx = new ProcedureDeclarationContext(this, this._ctx, this.state);
    this.enterRule(localctx, 8, LogoParser.RULE_procedureDeclaration);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 172;
        this.match(LogoParser.T__0);
        this.state = 173;
        this.name();
        this.state = 177;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===LogoParser.T__2) {
            this.state = 174;
            this.parameterDeclarations();
            this.state = 179;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 181;
        this._errHandler.sync(this);
        var la_ = this._interp.adaptivePredict(this._input,11,this._ctx);
        if(la_===1) {
            this.state = 180;
            this.match(LogoParser.EOL);

        }
        this.state = 187; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 184;
            this._errHandler.sync(this);
            var la_ = this._interp.adaptivePredict(this._input,12,this._ctx);
            if(la_===1) {
                this.state = 183;
                this.line();

            }
            this.state = 186;
            this.match(LogoParser.EOL);
            this.state = 189; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << LogoParser.T__0) | (1 << LogoParser.T__4) | (1 << LogoParser.T__7) | (1 << LogoParser.T__17) | (1 << LogoParser.T__18) | (1 << LogoParser.T__19) | (1 << LogoParser.T__29) | (1 << LogoParser.T__30))) !== 0) || ((((_la - 32)) & ~0x1f) == 0 && ((1 << (_la - 32)) & ((1 << (LogoParser.T__31 - 32)) | (1 << (LogoParser.T__32 - 32)) | (1 << (LogoParser.T__33 - 32)) | (1 << (LogoParser.T__34 - 32)) | (1 << (LogoParser.T__35 - 32)) | (1 << (LogoParser.T__36 - 32)) | (1 << (LogoParser.T__37 - 32)) | (1 << (LogoParser.T__38 - 32)) | (1 << (LogoParser.T__39 - 32)) | (1 << (LogoParser.T__40 - 32)) | (1 << (LogoParser.T__41 - 32)) | (1 << (LogoParser.T__42 - 32)) | (1 << (LogoParser.T__43 - 32)) | (1 << (LogoParser.T__44 - 32)) | (1 << (LogoParser.T__45 - 32)) | (1 << (LogoParser.T__46 - 32)) | (1 << (LogoParser.T__47 - 32)) | (1 << (LogoParser.T__48 - 32)) | (1 << (LogoParser.T__49 - 32)) | (1 << (LogoParser.T__50 - 32)) | (1 << (LogoParser.T__51 - 32)) | (1 << (LogoParser.T__52 - 32)) | (1 << (LogoParser.T__58 - 32)) | (1 << (LogoParser.T__59 - 32)) | (1 << (LogoParser.T__60 - 32)) | (1 << (LogoParser.T__61 - 32)) | (1 << (LogoParser.T__62 - 32)))) !== 0) || ((((_la - 65)) & ~0x1f) == 0 && ((1 << (_la - 65)) & ((1 << (LogoParser.T__64 - 65)) | (1 << (LogoParser.T__65 - 65)) | (1 << (LogoParser.T__66 - 65)) | (1 << (LogoParser.T__67 - 65)) | (1 << (LogoParser.T__68 - 65)) | (1 << (LogoParser.T__69 - 65)) | (1 << (LogoParser.T__70 - 65)) | (1 << (LogoParser.STRING - 65)) | (1 << (LogoParser.COMMENT - 65)) | (1 << (LogoParser.EOL - 65)))) !== 0));
        this.state = 191;
        this.match(LogoParser.T__1);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ParameterDeclarationsContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_parameterDeclarations;
    return this;
}

ParameterDeclarationsContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ParameterDeclarationsContext.prototype.constructor = ParameterDeclarationsContext;

ParameterDeclarationsContext.prototype.name = function() {
    return this.getTypedRuleContext(NameContext,0);
};

ParameterDeclarationsContext.prototype.parameterDeclarations = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ParameterDeclarationsContext);
    } else {
        return this.getTypedRuleContext(ParameterDeclarationsContext,i);
    }
};

ParameterDeclarationsContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterParameterDeclarations(this);
	}
};

ParameterDeclarationsContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitParameterDeclarations(this);
	}
};




LogoParser.ParameterDeclarationsContext = ParameterDeclarationsContext;

LogoParser.prototype.parameterDeclarations = function() {

    var localctx = new ParameterDeclarationsContext(this, this._ctx, this.state);
    this.enterRule(localctx, 10, LogoParser.RULE_parameterDeclarations);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 193;
        this.match(LogoParser.T__2);
        this.state = 194;
        this.name();
        this.state = 199;
        this._errHandler.sync(this);
        var _alt = this._interp.adaptivePredict(this._input,14,this._ctx)
        while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
            if(_alt===1) {
                this.state = 195;
                this.match(LogoParser.T__3);
                this.state = 196;
                this.parameterDeclarations(); 
            }
            this.state = 201;
            this._errHandler.sync(this);
            _alt = this._interp.adaptivePredict(this._input,14,this._ctx);
        }

    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function FuncContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_func;
    return this;
}

FuncContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
FuncContext.prototype.constructor = FuncContext;

FuncContext.prototype.random = function() {
    return this.getTypedRuleContext(RandomContext,0);
};

FuncContext.prototype.repcount = function() {
    return this.getTypedRuleContext(RepcountContext,0);
};

FuncContext.prototype.getangle = function() {
    return this.getTypedRuleContext(GetangleContext,0);
};

FuncContext.prototype.getx = function() {
    return this.getTypedRuleContext(GetxContext,0);
};

FuncContext.prototype.gety = function() {
    return this.getTypedRuleContext(GetyContext,0);
};

FuncContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterFunc(this);
	}
};

FuncContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitFunc(this);
	}
};




LogoParser.FuncContext = FuncContext;

LogoParser.prototype.func = function() {

    var localctx = new FuncContext(this, this._ctx, this.state);
    this.enterRule(localctx, 12, LogoParser.RULE_func);
    try {
        this.state = 207;
        this._errHandler.sync(this);
        switch(this._input.LA(1)) {
        case LogoParser.T__53:
            this.enterOuterAlt(localctx, 1);
            this.state = 202;
            this.random();
            break;
        case LogoParser.T__57:
            this.enterOuterAlt(localctx, 2);
            this.state = 203;
            this.repcount();
            break;
        case LogoParser.T__54:
            this.enterOuterAlt(localctx, 3);
            this.state = 204;
            this.getangle();
            break;
        case LogoParser.T__55:
            this.enterOuterAlt(localctx, 4);
            this.state = 205;
            this.getx();
            break;
        case LogoParser.T__56:
            this.enterOuterAlt(localctx, 5);
            this.state = 206;
            this.gety();
            break;
        default:
            throw new antlr4.error.NoViableAltException(this);
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function RepeatContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_repeat;
    return this;
}

RepeatContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
RepeatContext.prototype.constructor = RepeatContext;

RepeatContext.prototype.expression = function() {
    return this.getTypedRuleContext(ExpressionContext,0);
};

RepeatContext.prototype.block = function() {
    return this.getTypedRuleContext(BlockContext,0);
};

RepeatContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterRepeat(this);
	}
};

RepeatContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitRepeat(this);
	}
};




LogoParser.RepeatContext = RepeatContext;

LogoParser.prototype.repeat = function() {

    var localctx = new RepeatContext(this, this._ctx, this.state);
    this.enterRule(localctx, 14, LogoParser.RULE_repeat);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 209;
        this.match(LogoParser.T__4);
        this.state = 210;
        this.expression();
        this.state = 211;
        this.block();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function BlockContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_block;
    return this;
}

BlockContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
BlockContext.prototype.constructor = BlockContext;

BlockContext.prototype.line = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(LineContext);
    } else {
        return this.getTypedRuleContext(LineContext,i);
    }
};

BlockContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterBlock(this);
	}
};

BlockContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitBlock(this);
	}
};




LogoParser.BlockContext = BlockContext;

LogoParser.prototype.block = function() {

    var localctx = new BlockContext(this, this._ctx, this.state);
    this.enterRule(localctx, 16, LogoParser.RULE_block);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 213;
        this.match(LogoParser.T__5);
        this.state = 215; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 214;
            this.line();
            this.state = 217; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << LogoParser.T__0) | (1 << LogoParser.T__4) | (1 << LogoParser.T__7) | (1 << LogoParser.T__17) | (1 << LogoParser.T__18) | (1 << LogoParser.T__19) | (1 << LogoParser.T__29) | (1 << LogoParser.T__30))) !== 0) || ((((_la - 32)) & ~0x1f) == 0 && ((1 << (_la - 32)) & ((1 << (LogoParser.T__31 - 32)) | (1 << (LogoParser.T__32 - 32)) | (1 << (LogoParser.T__33 - 32)) | (1 << (LogoParser.T__34 - 32)) | (1 << (LogoParser.T__35 - 32)) | (1 << (LogoParser.T__36 - 32)) | (1 << (LogoParser.T__37 - 32)) | (1 << (LogoParser.T__38 - 32)) | (1 << (LogoParser.T__39 - 32)) | (1 << (LogoParser.T__40 - 32)) | (1 << (LogoParser.T__41 - 32)) | (1 << (LogoParser.T__42 - 32)) | (1 << (LogoParser.T__43 - 32)) | (1 << (LogoParser.T__44 - 32)) | (1 << (LogoParser.T__45 - 32)) | (1 << (LogoParser.T__46 - 32)) | (1 << (LogoParser.T__47 - 32)) | (1 << (LogoParser.T__48 - 32)) | (1 << (LogoParser.T__49 - 32)) | (1 << (LogoParser.T__50 - 32)) | (1 << (LogoParser.T__51 - 32)) | (1 << (LogoParser.T__52 - 32)) | (1 << (LogoParser.T__58 - 32)) | (1 << (LogoParser.T__59 - 32)) | (1 << (LogoParser.T__60 - 32)) | (1 << (LogoParser.T__61 - 32)) | (1 << (LogoParser.T__62 - 32)))) !== 0) || ((((_la - 65)) & ~0x1f) == 0 && ((1 << (_la - 65)) & ((1 << (LogoParser.T__64 - 65)) | (1 << (LogoParser.T__65 - 65)) | (1 << (LogoParser.T__66 - 65)) | (1 << (LogoParser.T__67 - 65)) | (1 << (LogoParser.T__68 - 65)) | (1 << (LogoParser.T__69 - 65)) | (1 << (LogoParser.T__70 - 65)) | (1 << (LogoParser.STRING - 65)) | (1 << (LogoParser.COMMENT - 65)) | (1 << (LogoParser.EOL - 65)))) !== 0));
        this.state = 219;
        this.match(LogoParser.T__6);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function IfeContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_ife;
    return this;
}

IfeContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
IfeContext.prototype.constructor = IfeContext;

IfeContext.prototype.comparison = function() {
    return this.getTypedRuleContext(ComparisonContext,0);
};

IfeContext.prototype.block = function() {
    return this.getTypedRuleContext(BlockContext,0);
};

IfeContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterIfe(this);
	}
};

IfeContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitIfe(this);
	}
};




LogoParser.IfeContext = IfeContext;

LogoParser.prototype.ife = function() {

    var localctx = new IfeContext(this, this._ctx, this.state);
    this.enterRule(localctx, 18, LogoParser.RULE_ife);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 221;
        this.match(LogoParser.T__7);
        this.state = 222;
        this.comparison();
        this.state = 223;
        this.block();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ComparisonContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_comparison;
    return this;
}

ComparisonContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ComparisonContext.prototype.constructor = ComparisonContext;

ComparisonContext.prototype.expression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ExpressionContext);
    } else {
        return this.getTypedRuleContext(ExpressionContext,i);
    }
};

ComparisonContext.prototype.comparisonOperator = function() {
    return this.getTypedRuleContext(ComparisonOperatorContext,0);
};

ComparisonContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterComparison(this);
	}
};

ComparisonContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitComparison(this);
	}
};




LogoParser.ComparisonContext = ComparisonContext;

LogoParser.prototype.comparison = function() {

    var localctx = new ComparisonContext(this, this._ctx, this.state);
    this.enterRule(localctx, 20, LogoParser.RULE_comparison);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 225;
        this.expression();
        this.state = 226;
        this.comparisonOperator();
        this.state = 227;
        this.expression();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ComparisonOperatorContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_comparisonOperator;
    return this;
}

ComparisonOperatorContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ComparisonOperatorContext.prototype.constructor = ComparisonOperatorContext;


ComparisonOperatorContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterComparisonOperator(this);
	}
};

ComparisonOperatorContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitComparisonOperator(this);
	}
};




LogoParser.ComparisonOperatorContext = ComparisonOperatorContext;

LogoParser.prototype.comparisonOperator = function() {

    var localctx = new ComparisonOperatorContext(this, this._ctx, this.state);
    this.enterRule(localctx, 22, LogoParser.RULE_comparisonOperator);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 229;
        _la = this._input.LA(1);
        if(!((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << LogoParser.T__8) | (1 << LogoParser.T__9) | (1 << LogoParser.T__10) | (1 << LogoParser.T__11) | (1 << LogoParser.T__12) | (1 << LogoParser.T__13) | (1 << LogoParser.T__14) | (1 << LogoParser.T__15) | (1 << LogoParser.T__16))) !== 0))) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function MakeContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_make;
    return this;
}

MakeContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
MakeContext.prototype.constructor = MakeContext;

MakeContext.prototype.STRINGLITERAL = function() {
    return this.getToken(LogoParser.STRINGLITERAL, 0);
};

MakeContext.prototype.value = function() {
    return this.getTypedRuleContext(ValueContext,0);
};

MakeContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterMake(this);
	}
};

MakeContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitMake(this);
	}
};




LogoParser.MakeContext = MakeContext;

LogoParser.prototype.make = function() {

    var localctx = new MakeContext(this, this._ctx, this.state);
    this.enterRule(localctx, 24, LogoParser.RULE_make);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 231;
        this.match(LogoParser.T__17);
        this.state = 232;
        this.match(LogoParser.STRINGLITERAL);
        this.state = 233;
        this.value();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function LocalmakeContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_localmake;
    return this;
}

LocalmakeContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
LocalmakeContext.prototype.constructor = LocalmakeContext;

LocalmakeContext.prototype.STRINGLITERAL = function() {
    return this.getToken(LogoParser.STRINGLITERAL, 0);
};

LocalmakeContext.prototype.value = function() {
    return this.getTypedRuleContext(ValueContext,0);
};

LocalmakeContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterLocalmake(this);
	}
};

LocalmakeContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitLocalmake(this);
	}
};




LogoParser.LocalmakeContext = LocalmakeContext;

LogoParser.prototype.localmake = function() {

    var localctx = new LocalmakeContext(this, this._ctx, this.state);
    this.enterRule(localctx, 26, LogoParser.RULE_localmake);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 235;
        this.match(LogoParser.T__18);
        this.state = 236;
        this.match(LogoParser.STRINGLITERAL);
        this.state = 237;
        this.value();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function Print_commandContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_print_command;
    return this;
}

Print_commandContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
Print_commandContext.prototype.constructor = Print_commandContext;

Print_commandContext.prototype.value = function() {
    return this.getTypedRuleContext(ValueContext,0);
};

Print_commandContext.prototype.quotedstring = function() {
    return this.getTypedRuleContext(QuotedstringContext,0);
};

Print_commandContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterPrint_command(this);
	}
};

Print_commandContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitPrint_command(this);
	}
};




LogoParser.Print_commandContext = Print_commandContext;

LogoParser.prototype.print_command = function() {

    var localctx = new Print_commandContext(this, this._ctx, this.state);
    this.enterRule(localctx, 28, LogoParser.RULE_print_command);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 239;
        this.match(LogoParser.T__19);
        this.state = 242;
        this._errHandler.sync(this);
        switch(this._input.LA(1)) {
        case LogoParser.T__2:
        case LogoParser.T__20:
        case LogoParser.T__22:
        case LogoParser.T__23:
        case LogoParser.T__53:
        case LogoParser.T__54:
        case LogoParser.T__55:
        case LogoParser.T__56:
        case LogoParser.T__57:
        case LogoParser.STRINGLITERAL:
        case LogoParser.NUMBER:
            this.state = 240;
            this.value();
            break;
        case LogoParser.T__5:
            this.state = 241;
            this.quotedstring();
            break;
        default:
            throw new antlr4.error.NoViableAltException(this);
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function QuotedstringContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_quotedstring;
    return this;
}

QuotedstringContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
QuotedstringContext.prototype.constructor = QuotedstringContext;

QuotedstringContext.prototype.quotedstring = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(QuotedstringContext);
    } else {
        return this.getTypedRuleContext(QuotedstringContext,i);
    }
};

QuotedstringContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterQuotedstring(this);
	}
};

QuotedstringContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitQuotedstring(this);
	}
};




LogoParser.QuotedstringContext = QuotedstringContext;

LogoParser.prototype.quotedstring = function() {

    var localctx = new QuotedstringContext(this, this._ctx, this.state);
    this.enterRule(localctx, 30, LogoParser.RULE_quotedstring);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 244;
        this.match(LogoParser.T__5);
        this.state = 249;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << LogoParser.T__0) | (1 << LogoParser.T__1) | (1 << LogoParser.T__2) | (1 << LogoParser.T__3) | (1 << LogoParser.T__4) | (1 << LogoParser.T__5) | (1 << LogoParser.T__7) | (1 << LogoParser.T__8) | (1 << LogoParser.T__9) | (1 << LogoParser.T__10) | (1 << LogoParser.T__11) | (1 << LogoParser.T__12) | (1 << LogoParser.T__13) | (1 << LogoParser.T__14) | (1 << LogoParser.T__15) | (1 << LogoParser.T__16) | (1 << LogoParser.T__17) | (1 << LogoParser.T__18) | (1 << LogoParser.T__19) | (1 << LogoParser.T__20) | (1 << LogoParser.T__21) | (1 << LogoParser.T__22) | (1 << LogoParser.T__23) | (1 << LogoParser.T__24) | (1 << LogoParser.T__25) | (1 << LogoParser.T__26) | (1 << LogoParser.T__27) | (1 << LogoParser.T__28) | (1 << LogoParser.T__29) | (1 << LogoParser.T__30))) !== 0) || ((((_la - 32)) & ~0x1f) == 0 && ((1 << (_la - 32)) & ((1 << (LogoParser.T__31 - 32)) | (1 << (LogoParser.T__32 - 32)) | (1 << (LogoParser.T__33 - 32)) | (1 << (LogoParser.T__34 - 32)) | (1 << (LogoParser.T__35 - 32)) | (1 << (LogoParser.T__36 - 32)) | (1 << (LogoParser.T__37 - 32)) | (1 << (LogoParser.T__38 - 32)) | (1 << (LogoParser.T__39 - 32)) | (1 << (LogoParser.T__40 - 32)) | (1 << (LogoParser.T__41 - 32)) | (1 << (LogoParser.T__42 - 32)) | (1 << (LogoParser.T__43 - 32)) | (1 << (LogoParser.T__44 - 32)) | (1 << (LogoParser.T__45 - 32)) | (1 << (LogoParser.T__46 - 32)) | (1 << (LogoParser.T__47 - 32)) | (1 << (LogoParser.T__48 - 32)) | (1 << (LogoParser.T__49 - 32)) | (1 << (LogoParser.T__50 - 32)) | (1 << (LogoParser.T__51 - 32)) | (1 << (LogoParser.T__52 - 32)) | (1 << (LogoParser.T__53 - 32)) | (1 << (LogoParser.T__54 - 32)) | (1 << (LogoParser.T__55 - 32)) | (1 << (LogoParser.T__56 - 32)) | (1 << (LogoParser.T__57 - 32)) | (1 << (LogoParser.T__58 - 32)) | (1 << (LogoParser.T__59 - 32)) | (1 << (LogoParser.T__60 - 32)) | (1 << (LogoParser.T__61 - 32)) | (1 << (LogoParser.T__62 - 32)))) !== 0) || ((((_la - 64)) & ~0x1f) == 0 && ((1 << (_la - 64)) & ((1 << (LogoParser.T__63 - 64)) | (1 << (LogoParser.T__64 - 64)) | (1 << (LogoParser.T__65 - 64)) | (1 << (LogoParser.T__66 - 64)) | (1 << (LogoParser.T__67 - 64)) | (1 << (LogoParser.T__68 - 64)) | (1 << (LogoParser.T__69 - 64)) | (1 << (LogoParser.T__70 - 64)) | (1 << (LogoParser.T__71 - 64)) | (1 << (LogoParser.T__72 - 64)) | (1 << (LogoParser.T__73 - 64)) | (1 << (LogoParser.T__74 - 64)) | (1 << (LogoParser.HEX - 64)) | (1 << (LogoParser.STRINGLITERAL - 64)) | (1 << (LogoParser.STRING - 64)) | (1 << (LogoParser.NUMBER - 64)) | (1 << (LogoParser.COMMENT - 64)) | (1 << (LogoParser.EOL - 64)) | (1 << (LogoParser.WS - 64)))) !== 0)) {
            this.state = 247;
            this._errHandler.sync(this);
            var la_ = this._interp.adaptivePredict(this._input,18,this._ctx);
            switch(la_) {
            case 1:
                this.state = 245;
                this.quotedstring();
                break;

            case 2:
                this.state = 246;
                _la = this._input.LA(1);
                if(_la<=0 || _la===LogoParser.T__6) {
                this._errHandler.recoverInline(this);
                }
                else {
                	this._errHandler.reportMatch(this);
                    this.consume();
                }
                break;

            }
            this.state = 251;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 252;
        this.match(LogoParser.T__6);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function NameContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_name;
    return this;
}

NameContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
NameContext.prototype.constructor = NameContext;

NameContext.prototype.STRING = function() {
    return this.getToken(LogoParser.STRING, 0);
};

NameContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterName(this);
	}
};

NameContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitName(this);
	}
};




LogoParser.NameContext = NameContext;

LogoParser.prototype.name = function() {

    var localctx = new NameContext(this, this._ctx, this.state);
    this.enterRule(localctx, 32, LogoParser.RULE_name);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 254;
        this.match(LogoParser.STRING);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ValueContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_value;
    return this;
}

ValueContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ValueContext.prototype.constructor = ValueContext;

ValueContext.prototype.STRINGLITERAL = function() {
    return this.getToken(LogoParser.STRINGLITERAL, 0);
};

ValueContext.prototype.expression = function() {
    return this.getTypedRuleContext(ExpressionContext,0);
};

ValueContext.prototype.deref = function() {
    return this.getTypedRuleContext(DerefContext,0);
};

ValueContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterValue(this);
	}
};

ValueContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitValue(this);
	}
};




LogoParser.ValueContext = ValueContext;

LogoParser.prototype.value = function() {

    var localctx = new ValueContext(this, this._ctx, this.state);
    this.enterRule(localctx, 34, LogoParser.RULE_value);
    try {
        this.state = 259;
        this._errHandler.sync(this);
        var la_ = this._interp.adaptivePredict(this._input,20,this._ctx);
        switch(la_) {
        case 1:
            this.enterOuterAlt(localctx, 1);
            this.state = 256;
            this.match(LogoParser.STRINGLITERAL);
            break;

        case 2:
            this.enterOuterAlt(localctx, 2);
            this.state = 257;
            this.expression();
            break;

        case 3:
            this.enterOuterAlt(localctx, 3);
            this.state = 258;
            this.deref();
            break;

        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ParenExpressionContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_parenExpression;
    return this;
}

ParenExpressionContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ParenExpressionContext.prototype.constructor = ParenExpressionContext;

ParenExpressionContext.prototype.expression = function() {
    return this.getTypedRuleContext(ExpressionContext,0);
};

ParenExpressionContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterParenExpression(this);
	}
};

ParenExpressionContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitParenExpression(this);
	}
};




LogoParser.ParenExpressionContext = ParenExpressionContext;

LogoParser.prototype.parenExpression = function() {

    var localctx = new ParenExpressionContext(this, this._ctx, this.state);
    this.enterRule(localctx, 36, LogoParser.RULE_parenExpression);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 261;
        this.match(LogoParser.T__20);
        this.state = 262;
        this.expression();
        this.state = 263;
        this.match(LogoParser.T__21);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function SignExpressionContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_signExpression;
    return this;
}

SignExpressionContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
SignExpressionContext.prototype.constructor = SignExpressionContext;

SignExpressionContext.prototype.number = function() {
    return this.getTypedRuleContext(NumberContext,0);
};

SignExpressionContext.prototype.deref = function() {
    return this.getTypedRuleContext(DerefContext,0);
};

SignExpressionContext.prototype.func = function() {
    return this.getTypedRuleContext(FuncContext,0);
};

SignExpressionContext.prototype.parenExpression = function() {
    return this.getTypedRuleContext(ParenExpressionContext,0);
};

SignExpressionContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterSignExpression(this);
	}
};

SignExpressionContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitSignExpression(this);
	}
};




LogoParser.SignExpressionContext = SignExpressionContext;

LogoParser.prototype.signExpression = function() {

    var localctx = new SignExpressionContext(this, this._ctx, this.state);
    this.enterRule(localctx, 38, LogoParser.RULE_signExpression);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 266;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        if(_la===LogoParser.T__22 || _la===LogoParser.T__23) {
            this.state = 265;
            _la = this._input.LA(1);
            if(!(_la===LogoParser.T__22 || _la===LogoParser.T__23)) {
            this._errHandler.recoverInline(this);
            }
            else {
            	this._errHandler.reportMatch(this);
                this.consume();
            }
        }

        this.state = 272;
        this._errHandler.sync(this);
        switch(this._input.LA(1)) {
        case LogoParser.NUMBER:
            this.state = 268;
            this.number();
            break;
        case LogoParser.T__2:
            this.state = 269;
            this.deref();
            break;
        case LogoParser.T__53:
        case LogoParser.T__54:
        case LogoParser.T__55:
        case LogoParser.T__56:
        case LogoParser.T__57:
            this.state = 270;
            this.func();
            break;
        case LogoParser.T__20:
            this.state = 271;
            this.parenExpression();
            break;
        default:
            throw new antlr4.error.NoViableAltException(this);
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function PowerExpressionContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_powerExpression;
    return this;
}

PowerExpressionContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
PowerExpressionContext.prototype.constructor = PowerExpressionContext;

PowerExpressionContext.prototype.signExpression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(SignExpressionContext);
    } else {
        return this.getTypedRuleContext(SignExpressionContext,i);
    }
};

PowerExpressionContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterPowerExpression(this);
	}
};

PowerExpressionContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitPowerExpression(this);
	}
};




LogoParser.PowerExpressionContext = PowerExpressionContext;

LogoParser.prototype.powerExpression = function() {

    var localctx = new PowerExpressionContext(this, this._ctx, this.state);
    this.enterRule(localctx, 40, LogoParser.RULE_powerExpression);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 274;
        this.signExpression();
        this.state = 277;
        this._errHandler.sync(this);
        var la_ = this._interp.adaptivePredict(this._input,23,this._ctx);
        if(la_===1) {
            this.state = 275;
            this.match(LogoParser.T__24);
            this.state = 276;
            this.signExpression();

        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function MultiplyingExpressionContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_multiplyingExpression;
    return this;
}

MultiplyingExpressionContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
MultiplyingExpressionContext.prototype.constructor = MultiplyingExpressionContext;

MultiplyingExpressionContext.prototype.powerExpression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(PowerExpressionContext);
    } else {
        return this.getTypedRuleContext(PowerExpressionContext,i);
    }
};

MultiplyingExpressionContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterMultiplyingExpression(this);
	}
};

MultiplyingExpressionContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitMultiplyingExpression(this);
	}
};




LogoParser.MultiplyingExpressionContext = MultiplyingExpressionContext;

LogoParser.prototype.multiplyingExpression = function() {

    var localctx = new MultiplyingExpressionContext(this, this._ctx, this.state);
    this.enterRule(localctx, 42, LogoParser.RULE_multiplyingExpression);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 279;
        this.powerExpression();
        this.state = 284;
        this._errHandler.sync(this);
        var _alt = this._interp.adaptivePredict(this._input,24,this._ctx)
        while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
            if(_alt===1) {
                this.state = 280;
                _la = this._input.LA(1);
                if(!((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << LogoParser.T__25) | (1 << LogoParser.T__26) | (1 << LogoParser.T__27) | (1 << LogoParser.T__28))) !== 0))) {
                this._errHandler.recoverInline(this);
                }
                else {
                	this._errHandler.reportMatch(this);
                    this.consume();
                }
                this.state = 281;
                this.powerExpression(); 
            }
            this.state = 286;
            this._errHandler.sync(this);
            _alt = this._interp.adaptivePredict(this._input,24,this._ctx);
        }

    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ExpressionContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_expression;
    return this;
}

ExpressionContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ExpressionContext.prototype.constructor = ExpressionContext;

ExpressionContext.prototype.multiplyingExpression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(MultiplyingExpressionContext);
    } else {
        return this.getTypedRuleContext(MultiplyingExpressionContext,i);
    }
};

ExpressionContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterExpression(this);
	}
};

ExpressionContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitExpression(this);
	}
};




LogoParser.ExpressionContext = ExpressionContext;

LogoParser.prototype.expression = function() {

    var localctx = new ExpressionContext(this, this._ctx, this.state);
    this.enterRule(localctx, 44, LogoParser.RULE_expression);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 287;
        this.multiplyingExpression();
        this.state = 292;
        this._errHandler.sync(this);
        var _alt = this._interp.adaptivePredict(this._input,25,this._ctx)
        while(_alt!=2 && _alt!=antlr4.atn.ATN.INVALID_ALT_NUMBER) {
            if(_alt===1) {
                this.state = 288;
                _la = this._input.LA(1);
                if(!(_la===LogoParser.T__22 || _la===LogoParser.T__23)) {
                this._errHandler.recoverInline(this);
                }
                else {
                	this._errHandler.reportMatch(this);
                    this.consume();
                }
                this.state = 289;
                this.multiplyingExpression(); 
            }
            this.state = 294;
            this._errHandler.sync(this);
            _alt = this._interp.adaptivePredict(this._input,25,this._ctx);
        }

    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function DerefContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_deref;
    return this;
}

DerefContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
DerefContext.prototype.constructor = DerefContext;

DerefContext.prototype.name = function() {
    return this.getTypedRuleContext(NameContext,0);
};

DerefContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterDeref(this);
	}
};

DerefContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitDeref(this);
	}
};




LogoParser.DerefContext = DerefContext;

LogoParser.prototype.deref = function() {

    var localctx = new DerefContext(this, this._ctx, this.state);
    this.enterRule(localctx, 46, LogoParser.RULE_deref);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 295;
        this.match(LogoParser.T__2);
        this.state = 296;
        this.name();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function FdContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_fd;
    return this;
}

FdContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
FdContext.prototype.constructor = FdContext;

FdContext.prototype.expression = function() {
    return this.getTypedRuleContext(ExpressionContext,0);
};

FdContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterFd(this);
	}
};

FdContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitFd(this);
	}
};




LogoParser.FdContext = FdContext;

LogoParser.prototype.fd = function() {

    var localctx = new FdContext(this, this._ctx, this.state);
    this.enterRule(localctx, 48, LogoParser.RULE_fd);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 298;
        _la = this._input.LA(1);
        if(!(_la===LogoParser.T__29 || _la===LogoParser.T__30)) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
        this.state = 299;
        this.expression();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function BkContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_bk;
    return this;
}

BkContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
BkContext.prototype.constructor = BkContext;

BkContext.prototype.expression = function() {
    return this.getTypedRuleContext(ExpressionContext,0);
};

BkContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterBk(this);
	}
};

BkContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitBk(this);
	}
};




LogoParser.BkContext = BkContext;

LogoParser.prototype.bk = function() {

    var localctx = new BkContext(this, this._ctx, this.state);
    this.enterRule(localctx, 50, LogoParser.RULE_bk);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 301;
        _la = this._input.LA(1);
        if(!(((((_la - 32)) & ~0x1f) == 0 && ((1 << (_la - 32)) & ((1 << (LogoParser.T__31 - 32)) | (1 << (LogoParser.T__32 - 32)) | (1 << (LogoParser.T__33 - 32)))) !== 0))) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
        this.state = 302;
        this.expression();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function RtContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_rt;
    return this;
}

RtContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
RtContext.prototype.constructor = RtContext;

RtContext.prototype.expression = function() {
    return this.getTypedRuleContext(ExpressionContext,0);
};

RtContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterRt(this);
	}
};

RtContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitRt(this);
	}
};




LogoParser.RtContext = RtContext;

LogoParser.prototype.rt = function() {

    var localctx = new RtContext(this, this._ctx, this.state);
    this.enterRule(localctx, 52, LogoParser.RULE_rt);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 304;
        _la = this._input.LA(1);
        if(!(_la===LogoParser.T__34 || _la===LogoParser.T__35)) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
        this.state = 305;
        this.expression();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function LtContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_lt;
    return this;
}

LtContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
LtContext.prototype.constructor = LtContext;

LtContext.prototype.expression = function() {
    return this.getTypedRuleContext(ExpressionContext,0);
};

LtContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterLt(this);
	}
};

LtContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitLt(this);
	}
};




LogoParser.LtContext = LtContext;

LogoParser.prototype.lt = function() {

    var localctx = new LtContext(this, this._ctx, this.state);
    this.enterRule(localctx, 54, LogoParser.RULE_lt);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 307;
        _la = this._input.LA(1);
        if(!(_la===LogoParser.T__36 || _la===LogoParser.T__37)) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
        this.state = 308;
        this.expression();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function CsContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_cs;
    return this;
}

CsContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
CsContext.prototype.constructor = CsContext;


CsContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterCs(this);
	}
};

CsContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitCs(this);
	}
};




LogoParser.CsContext = CsContext;

LogoParser.prototype.cs = function() {

    var localctx = new CsContext(this, this._ctx, this.state);
    this.enterRule(localctx, 56, LogoParser.RULE_cs);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 310;
        _la = this._input.LA(1);
        if(!(((((_la - 39)) & ~0x1f) == 0 && ((1 << (_la - 39)) & ((1 << (LogoParser.T__38 - 39)) | (1 << (LogoParser.T__39 - 39)) | (1 << (LogoParser.T__40 - 39)) | (1 << (LogoParser.T__41 - 39)))) !== 0))) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function PuContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_pu;
    return this;
}

PuContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
PuContext.prototype.constructor = PuContext;


PuContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterPu(this);
	}
};

PuContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitPu(this);
	}
};




LogoParser.PuContext = PuContext;

LogoParser.prototype.pu = function() {

    var localctx = new PuContext(this, this._ctx, this.state);
    this.enterRule(localctx, 58, LogoParser.RULE_pu);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 312;
        _la = this._input.LA(1);
        if(!(_la===LogoParser.T__42 || _la===LogoParser.T__43)) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function PdContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_pd;
    return this;
}

PdContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
PdContext.prototype.constructor = PdContext;


PdContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterPd(this);
	}
};

PdContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitPd(this);
	}
};




LogoParser.PdContext = PdContext;

LogoParser.prototype.pd = function() {

    var localctx = new PdContext(this, this._ctx, this.state);
    this.enterRule(localctx, 60, LogoParser.RULE_pd);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 314;
        _la = this._input.LA(1);
        if(!(_la===LogoParser.T__44 || _la===LogoParser.T__45)) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function HtContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_ht;
    return this;
}

HtContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
HtContext.prototype.constructor = HtContext;


HtContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterHt(this);
	}
};

HtContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitHt(this);
	}
};




LogoParser.HtContext = HtContext;

LogoParser.prototype.ht = function() {

    var localctx = new HtContext(this, this._ctx, this.state);
    this.enterRule(localctx, 62, LogoParser.RULE_ht);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 316;
        _la = this._input.LA(1);
        if(!(_la===LogoParser.T__46 || _la===LogoParser.T__47)) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function StContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_st;
    return this;
}

StContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
StContext.prototype.constructor = StContext;


StContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterSt(this);
	}
};

StContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitSt(this);
	}
};




LogoParser.StContext = StContext;

LogoParser.prototype.st = function() {

    var localctx = new StContext(this, this._ctx, this.state);
    this.enterRule(localctx, 64, LogoParser.RULE_st);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 318;
        _la = this._input.LA(1);
        if(!(_la===LogoParser.T__48 || _la===LogoParser.T__49)) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function HomeContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_home;
    return this;
}

HomeContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
HomeContext.prototype.constructor = HomeContext;


HomeContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterHome(this);
	}
};

HomeContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitHome(this);
	}
};




LogoParser.HomeContext = HomeContext;

LogoParser.prototype.home = function() {

    var localctx = new HomeContext(this, this._ctx, this.state);
    this.enterRule(localctx, 66, LogoParser.RULE_home);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 320;
        this.match(LogoParser.T__50);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function StopContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_stop;
    return this;
}

StopContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
StopContext.prototype.constructor = StopContext;


StopContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterStop(this);
	}
};

StopContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitStop(this);
	}
};




LogoParser.StopContext = StopContext;

LogoParser.prototype.stop = function() {

    var localctx = new StopContext(this, this._ctx, this.state);
    this.enterRule(localctx, 68, LogoParser.RULE_stop);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 322;
        this.match(LogoParser.T__51);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function SetxyContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_setxy;
    return this;
}

SetxyContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
SetxyContext.prototype.constructor = SetxyContext;

SetxyContext.prototype.expression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ExpressionContext);
    } else {
        return this.getTypedRuleContext(ExpressionContext,i);
    }
};

SetxyContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterSetxy(this);
	}
};

SetxyContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitSetxy(this);
	}
};




LogoParser.SetxyContext = SetxyContext;

LogoParser.prototype.setxy = function() {

    var localctx = new SetxyContext(this, this._ctx, this.state);
    this.enterRule(localctx, 70, LogoParser.RULE_setxy);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 324;
        this.match(LogoParser.T__52);
        this.state = 325;
        this.expression();
        this.state = 326;
        this.expression();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function RandomContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_random;
    return this;
}

RandomContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
RandomContext.prototype.constructor = RandomContext;

RandomContext.prototype.expression = function() {
    return this.getTypedRuleContext(ExpressionContext,0);
};

RandomContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterRandom(this);
	}
};

RandomContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitRandom(this);
	}
};




LogoParser.RandomContext = RandomContext;

LogoParser.prototype.random = function() {

    var localctx = new RandomContext(this, this._ctx, this.state);
    this.enterRule(localctx, 72, LogoParser.RULE_random);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 328;
        this.match(LogoParser.T__53);
        this.state = 329;
        this.expression();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function GetangleContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_getangle;
    return this;
}

GetangleContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
GetangleContext.prototype.constructor = GetangleContext;


GetangleContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterGetangle(this);
	}
};

GetangleContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitGetangle(this);
	}
};




LogoParser.GetangleContext = GetangleContext;

LogoParser.prototype.getangle = function() {

    var localctx = new GetangleContext(this, this._ctx, this.state);
    this.enterRule(localctx, 74, LogoParser.RULE_getangle);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 331;
        this.match(LogoParser.T__54);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function GetxContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_getx;
    return this;
}

GetxContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
GetxContext.prototype.constructor = GetxContext;


GetxContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterGetx(this);
	}
};

GetxContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitGetx(this);
	}
};




LogoParser.GetxContext = GetxContext;

LogoParser.prototype.getx = function() {

    var localctx = new GetxContext(this, this._ctx, this.state);
    this.enterRule(localctx, 76, LogoParser.RULE_getx);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 333;
        this.match(LogoParser.T__55);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function GetyContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_gety;
    return this;
}

GetyContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
GetyContext.prototype.constructor = GetyContext;


GetyContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterGety(this);
	}
};

GetyContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitGety(this);
	}
};




LogoParser.GetyContext = GetyContext;

LogoParser.prototype.gety = function() {

    var localctx = new GetyContext(this, this._ctx, this.state);
    this.enterRule(localctx, 78, LogoParser.RULE_gety);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 335;
        this.match(LogoParser.T__56);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function RepcountContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_repcount;
    return this;
}

RepcountContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
RepcountContext.prototype.constructor = RepcountContext;


RepcountContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterRepcount(this);
	}
};

RepcountContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitRepcount(this);
	}
};




LogoParser.RepcountContext = RepcountContext;

LogoParser.prototype.repcount = function() {

    var localctx = new RepcountContext(this, this._ctx, this.state);
    this.enterRule(localctx, 80, LogoParser.RULE_repcount);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 337;
        this.match(LogoParser.T__57);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ForeContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_fore;
    return this;
}

ForeContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ForeContext.prototype.constructor = ForeContext;

ForeContext.prototype.name = function() {
    return this.getTypedRuleContext(NameContext,0);
};

ForeContext.prototype.expression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ExpressionContext);
    } else {
        return this.getTypedRuleContext(ExpressionContext,i);
    }
};

ForeContext.prototype.block = function() {
    return this.getTypedRuleContext(BlockContext,0);
};

ForeContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterFore(this);
	}
};

ForeContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitFore(this);
	}
};




LogoParser.ForeContext = ForeContext;

LogoParser.prototype.fore = function() {

    var localctx = new ForeContext(this, this._ctx, this.state);
    this.enterRule(localctx, 82, LogoParser.RULE_fore);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 339;
        this.match(LogoParser.T__58);
        this.state = 340;
        this.match(LogoParser.T__5);
        this.state = 341;
        this.name();
        this.state = 342;
        this.expression();
        this.state = 343;
        this.expression();
        this.state = 345;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        if((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << LogoParser.T__2) | (1 << LogoParser.T__20) | (1 << LogoParser.T__22) | (1 << LogoParser.T__23))) !== 0) || ((((_la - 54)) & ~0x1f) == 0 && ((1 << (_la - 54)) & ((1 << (LogoParser.T__53 - 54)) | (1 << (LogoParser.T__54 - 54)) | (1 << (LogoParser.T__55 - 54)) | (1 << (LogoParser.T__56 - 54)) | (1 << (LogoParser.T__57 - 54)) | (1 << (LogoParser.NUMBER - 54)))) !== 0)) {
            this.state = 344;
            this.expression();
        }

        this.state = 347;
        this.match(LogoParser.T__6);
        this.state = 348;
        this.block();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function PcContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_pc;
    return this;
}

PcContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
PcContext.prototype.constructor = PcContext;

PcContext.prototype.name = function() {
    return this.getTypedRuleContext(NameContext,0);
};

PcContext.prototype.expression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ExpressionContext);
    } else {
        return this.getTypedRuleContext(ExpressionContext,i);
    }
};

PcContext.prototype.hexcolor = function() {
    return this.getTypedRuleContext(HexcolorContext,0);
};

PcContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterPc(this);
	}
};

PcContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitPc(this);
	}
};




LogoParser.PcContext = PcContext;

LogoParser.prototype.pc = function() {

    var localctx = new PcContext(this, this._ctx, this.state);
    this.enterRule(localctx, 84, LogoParser.RULE_pc);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 350;
        _la = this._input.LA(1);
        if(!(_la===LogoParser.T__59 || _la===LogoParser.T__60)) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
        this.state = 359;
        this._errHandler.sync(this);
        switch(this._input.LA(1)) {
        case LogoParser.STRING:
            this.state = 351;
            this.name();
            break;
        case LogoParser.T__2:
        case LogoParser.T__20:
        case LogoParser.T__22:
        case LogoParser.T__23:
        case LogoParser.T__53:
        case LogoParser.T__54:
        case LogoParser.T__55:
        case LogoParser.T__56:
        case LogoParser.T__57:
        case LogoParser.NUMBER:
            this.state = 352;
            this.expression();
            this.state = 353;
            this.expression();
            this.state = 354;
            this.expression();
            this.state = 356;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
            if((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << LogoParser.T__2) | (1 << LogoParser.T__20) | (1 << LogoParser.T__22) | (1 << LogoParser.T__23))) !== 0) || ((((_la - 54)) & ~0x1f) == 0 && ((1 << (_la - 54)) & ((1 << (LogoParser.T__53 - 54)) | (1 << (LogoParser.T__54 - 54)) | (1 << (LogoParser.T__55 - 54)) | (1 << (LogoParser.T__56 - 54)) | (1 << (LogoParser.T__57 - 54)) | (1 << (LogoParser.NUMBER - 54)))) !== 0)) {
                this.state = 355;
                this.expression();
            }

            break;
        case LogoParser.T__63:
            this.state = 358;
            this.hexcolor();
            break;
        default:
            throw new antlr4.error.NoViableAltException(this);
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function CcContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_cc;
    return this;
}

CcContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
CcContext.prototype.constructor = CcContext;

CcContext.prototype.name = function() {
    return this.getTypedRuleContext(NameContext,0);
};

CcContext.prototype.expression = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ExpressionContext);
    } else {
        return this.getTypedRuleContext(ExpressionContext,i);
    }
};

CcContext.prototype.hexcolor = function() {
    return this.getTypedRuleContext(HexcolorContext,0);
};

CcContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterCc(this);
	}
};

CcContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitCc(this);
	}
};




LogoParser.CcContext = CcContext;

LogoParser.prototype.cc = function() {

    var localctx = new CcContext(this, this._ctx, this.state);
    this.enterRule(localctx, 86, LogoParser.RULE_cc);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 361;
        _la = this._input.LA(1);
        if(!(_la===LogoParser.T__61 || _la===LogoParser.T__62)) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
        this.state = 368;
        this._errHandler.sync(this);
        switch(this._input.LA(1)) {
        case LogoParser.STRING:
            this.state = 362;
            this.name();
            break;
        case LogoParser.T__2:
        case LogoParser.T__20:
        case LogoParser.T__22:
        case LogoParser.T__23:
        case LogoParser.T__53:
        case LogoParser.T__54:
        case LogoParser.T__55:
        case LogoParser.T__56:
        case LogoParser.T__57:
        case LogoParser.NUMBER:
            this.state = 363;
            this.expression();
            this.state = 364;
            this.expression();
            this.state = 365;
            this.expression();
            break;
        case LogoParser.T__63:
            this.state = 367;
            this.hexcolor();
            break;
        default:
            throw new antlr4.error.NoViableAltException(this);
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function HexcolorContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_hexcolor;
    return this;
}

HexcolorContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
HexcolorContext.prototype.constructor = HexcolorContext;

HexcolorContext.prototype.HEX = function() {
    return this.getToken(LogoParser.HEX, 0);
};

HexcolorContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterHexcolor(this);
	}
};

HexcolorContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitHexcolor(this);
	}
};




LogoParser.HexcolorContext = HexcolorContext;

LogoParser.prototype.hexcolor = function() {

    var localctx = new HexcolorContext(this, this._ctx, this.state);
    this.enterRule(localctx, 88, LogoParser.RULE_hexcolor);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 370;
        this.match(LogoParser.T__63);
        this.state = 371;
        this.match(LogoParser.HEX);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function PauseContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_pause;
    return this;
}

PauseContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
PauseContext.prototype.constructor = PauseContext;

PauseContext.prototype.expression = function() {
    return this.getTypedRuleContext(ExpressionContext,0);
};

PauseContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterPause(this);
	}
};

PauseContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitPause(this);
	}
};




LogoParser.PauseContext = PauseContext;

LogoParser.prototype.pause = function() {

    var localctx = new PauseContext(this, this._ctx, this.state);
    this.enterRule(localctx, 90, LogoParser.RULE_pause);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 373;
        this.match(LogoParser.T__64);
        this.state = 374;
        this.expression();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function DsContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_ds;
    return this;
}

DsContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
DsContext.prototype.constructor = DsContext;

DsContext.prototype.value = function() {
    return this.getTypedRuleContext(ValueContext,0);
};

DsContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterDs(this);
	}
};

DsContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitDs(this);
	}
};




LogoParser.DsContext = DsContext;

LogoParser.prototype.ds = function() {

    var localctx = new DsContext(this, this._ctx, this.state);
    this.enterRule(localctx, 92, LogoParser.RULE_ds);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 376;
        _la = this._input.LA(1);
        if(!(((((_la - 66)) & ~0x1f) == 0 && ((1 << (_la - 66)) & ((1 << (LogoParser.T__65 - 66)) | (1 << (LogoParser.T__66 - 66)) | (1 << (LogoParser.T__67 - 66)))) !== 0))) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
        this.state = 377;
        this.value();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function FontnameContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_fontname;
    return this;
}

FontnameContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
FontnameContext.prototype.constructor = FontnameContext;

FontnameContext.prototype.name = function() {
    return this.getTypedRuleContext(NameContext,0);
};

FontnameContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterFontname(this);
	}
};

FontnameContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitFontname(this);
	}
};




LogoParser.FontnameContext = FontnameContext;

LogoParser.prototype.fontname = function() {

    var localctx = new FontnameContext(this, this._ctx, this.state);
    this.enterRule(localctx, 94, LogoParser.RULE_fontname);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 379;
        this.match(LogoParser.T__68);
        this.state = 380;
        this.name();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function FontsizeContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_fontsize;
    return this;
}

FontsizeContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
FontsizeContext.prototype.constructor = FontsizeContext;

FontsizeContext.prototype.expression = function() {
    return this.getTypedRuleContext(ExpressionContext,0);
};

FontsizeContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterFontsize(this);
	}
};

FontsizeContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitFontsize(this);
	}
};




LogoParser.FontsizeContext = FontsizeContext;

LogoParser.prototype.fontsize = function() {

    var localctx = new FontsizeContext(this, this._ctx, this.state);
    this.enterRule(localctx, 96, LogoParser.RULE_fontsize);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 382;
        this.match(LogoParser.T__69);
        this.state = 383;
        this.expression();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function FontstyleContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_fontstyle;
    return this;
}

FontstyleContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
FontstyleContext.prototype.constructor = FontstyleContext;

FontstyleContext.prototype.style = function() {
    return this.getTypedRuleContext(StyleContext,0);
};

FontstyleContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterFontstyle(this);
	}
};

FontstyleContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitFontstyle(this);
	}
};




LogoParser.FontstyleContext = FontstyleContext;

LogoParser.prototype.fontstyle = function() {

    var localctx = new FontstyleContext(this, this._ctx, this.state);
    this.enterRule(localctx, 98, LogoParser.RULE_fontstyle);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 385;
        this.match(LogoParser.T__70);
        this.state = 386;
        this.style();
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function StyleContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_style;
    return this;
}

StyleContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
StyleContext.prototype.constructor = StyleContext;


StyleContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterStyle(this);
	}
};

StyleContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitStyle(this);
	}
};




LogoParser.StyleContext = StyleContext;

LogoParser.prototype.style = function() {

    var localctx = new StyleContext(this, this._ctx, this.state);
    this.enterRule(localctx, 100, LogoParser.RULE_style);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 388;
        _la = this._input.LA(1);
        if(!(((((_la - 72)) & ~0x1f) == 0 && ((1 << (_la - 72)) & ((1 << (LogoParser.T__71 - 72)) | (1 << (LogoParser.T__72 - 72)) | (1 << (LogoParser.T__73 - 72)) | (1 << (LogoParser.T__74 - 72)))) !== 0))) {
        this._errHandler.recoverInline(this);
        }
        else {
        	this._errHandler.reportMatch(this);
            this.consume();
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function NumberContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_number;
    return this;
}

NumberContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
NumberContext.prototype.constructor = NumberContext;

NumberContext.prototype.NUMBER = function() {
    return this.getToken(LogoParser.NUMBER, 0);
};

NumberContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterNumber(this);
	}
};

NumberContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitNumber(this);
	}
};




LogoParser.NumberContext = NumberContext;

LogoParser.prototype.number = function() {

    var localctx = new NumberContext(this, this._ctx, this.state);
    this.enterRule(localctx, 102, LogoParser.RULE_number);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 390;
        this.match(LogoParser.NUMBER);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function CommentContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = LogoParser.RULE_comment;
    return this;
}

CommentContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
CommentContext.prototype.constructor = CommentContext;

CommentContext.prototype.COMMENT = function() {
    return this.getToken(LogoParser.COMMENT, 0);
};

CommentContext.prototype.enterRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.enterComment(this);
	}
};

CommentContext.prototype.exitRule = function(listener) {
    if(listener instanceof LogoListener ) {
        listener.exitComment(this);
	}
};




LogoParser.CommentContext = CommentContext;

LogoParser.prototype.comment = function() {

    var localctx = new CommentContext(this, this._ctx, this.state);
    this.enterRule(localctx, 104, LogoParser.RULE_comment);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 392;
        this.match(LogoParser.COMMENT);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


exports.LogoParser = LogoParser;
