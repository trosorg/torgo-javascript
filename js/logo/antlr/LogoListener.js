// Generated from Logo.g4 by ANTLR 4.7
// jshint ignore: start
var antlr4 = require('antlr4/index');

// This class defines a complete listener for a parse tree produced by LogoParser.
function LogoListener() {
	antlr4.tree.ParseTreeListener.call(this);
	return this;
}

LogoListener.prototype = Object.create(antlr4.tree.ParseTreeListener.prototype);
LogoListener.prototype.constructor = LogoListener;

// Enter a parse tree produced by LogoParser#prog.
LogoListener.prototype.enterProg = function(ctx) {
};

// Exit a parse tree produced by LogoParser#prog.
LogoListener.prototype.exitProg = function(ctx) {
};


// Enter a parse tree produced by LogoParser#line.
LogoListener.prototype.enterLine = function(ctx) {
};

// Exit a parse tree produced by LogoParser#line.
LogoListener.prototype.exitLine = function(ctx) {
};


// Enter a parse tree produced by LogoParser#cmd.
LogoListener.prototype.enterCmd = function(ctx) {
};

// Exit a parse tree produced by LogoParser#cmd.
LogoListener.prototype.exitCmd = function(ctx) {
};


// Enter a parse tree produced by LogoParser#procedureInvocation.
LogoListener.prototype.enterProcedureInvocation = function(ctx) {
};

// Exit a parse tree produced by LogoParser#procedureInvocation.
LogoListener.prototype.exitProcedureInvocation = function(ctx) {
};


// Enter a parse tree produced by LogoParser#procedureDeclaration.
LogoListener.prototype.enterProcedureDeclaration = function(ctx) {
};

// Exit a parse tree produced by LogoParser#procedureDeclaration.
LogoListener.prototype.exitProcedureDeclaration = function(ctx) {
};


// Enter a parse tree produced by LogoParser#parameterDeclarations.
LogoListener.prototype.enterParameterDeclarations = function(ctx) {
};

// Exit a parse tree produced by LogoParser#parameterDeclarations.
LogoListener.prototype.exitParameterDeclarations = function(ctx) {
};


// Enter a parse tree produced by LogoParser#func.
LogoListener.prototype.enterFunc = function(ctx) {
};

// Exit a parse tree produced by LogoParser#func.
LogoListener.prototype.exitFunc = function(ctx) {
};


// Enter a parse tree produced by LogoParser#repeat.
LogoListener.prototype.enterRepeat = function(ctx) {
};

// Exit a parse tree produced by LogoParser#repeat.
LogoListener.prototype.exitRepeat = function(ctx) {
};


// Enter a parse tree produced by LogoParser#block.
LogoListener.prototype.enterBlock = function(ctx) {
};

// Exit a parse tree produced by LogoParser#block.
LogoListener.prototype.exitBlock = function(ctx) {
};


// Enter a parse tree produced by LogoParser#ife.
LogoListener.prototype.enterIfe = function(ctx) {
};

// Exit a parse tree produced by LogoParser#ife.
LogoListener.prototype.exitIfe = function(ctx) {
};


// Enter a parse tree produced by LogoParser#comparison.
LogoListener.prototype.enterComparison = function(ctx) {
};

// Exit a parse tree produced by LogoParser#comparison.
LogoListener.prototype.exitComparison = function(ctx) {
};


// Enter a parse tree produced by LogoParser#comparisonOperator.
LogoListener.prototype.enterComparisonOperator = function(ctx) {
};

// Exit a parse tree produced by LogoParser#comparisonOperator.
LogoListener.prototype.exitComparisonOperator = function(ctx) {
};


// Enter a parse tree produced by LogoParser#make.
LogoListener.prototype.enterMake = function(ctx) {
};

// Exit a parse tree produced by LogoParser#make.
LogoListener.prototype.exitMake = function(ctx) {
};


// Enter a parse tree produced by LogoParser#localmake.
LogoListener.prototype.enterLocalmake = function(ctx) {
};

// Exit a parse tree produced by LogoParser#localmake.
LogoListener.prototype.exitLocalmake = function(ctx) {
};


// Enter a parse tree produced by LogoParser#print_command.
LogoListener.prototype.enterPrint_command = function(ctx) {
};

// Exit a parse tree produced by LogoParser#print_command.
LogoListener.prototype.exitPrint_command = function(ctx) {
};


// Enter a parse tree produced by LogoParser#quotedstring.
LogoListener.prototype.enterQuotedstring = function(ctx) {
};

// Exit a parse tree produced by LogoParser#quotedstring.
LogoListener.prototype.exitQuotedstring = function(ctx) {
};


// Enter a parse tree produced by LogoParser#name.
LogoListener.prototype.enterName = function(ctx) {
};

// Exit a parse tree produced by LogoParser#name.
LogoListener.prototype.exitName = function(ctx) {
};


// Enter a parse tree produced by LogoParser#value.
LogoListener.prototype.enterValue = function(ctx) {
};

// Exit a parse tree produced by LogoParser#value.
LogoListener.prototype.exitValue = function(ctx) {
};


// Enter a parse tree produced by LogoParser#parenExpression.
LogoListener.prototype.enterParenExpression = function(ctx) {
};

// Exit a parse tree produced by LogoParser#parenExpression.
LogoListener.prototype.exitParenExpression = function(ctx) {
};


// Enter a parse tree produced by LogoParser#signExpression.
LogoListener.prototype.enterSignExpression = function(ctx) {
};

// Exit a parse tree produced by LogoParser#signExpression.
LogoListener.prototype.exitSignExpression = function(ctx) {
};


// Enter a parse tree produced by LogoParser#powerExpression.
LogoListener.prototype.enterPowerExpression = function(ctx) {
};

// Exit a parse tree produced by LogoParser#powerExpression.
LogoListener.prototype.exitPowerExpression = function(ctx) {
};


// Enter a parse tree produced by LogoParser#multiplyingExpression.
LogoListener.prototype.enterMultiplyingExpression = function(ctx) {
};

// Exit a parse tree produced by LogoParser#multiplyingExpression.
LogoListener.prototype.exitMultiplyingExpression = function(ctx) {
};


// Enter a parse tree produced by LogoParser#expression.
LogoListener.prototype.enterExpression = function(ctx) {
};

// Exit a parse tree produced by LogoParser#expression.
LogoListener.prototype.exitExpression = function(ctx) {
};


// Enter a parse tree produced by LogoParser#deref.
LogoListener.prototype.enterDeref = function(ctx) {
};

// Exit a parse tree produced by LogoParser#deref.
LogoListener.prototype.exitDeref = function(ctx) {
};


// Enter a parse tree produced by LogoParser#fd.
LogoListener.prototype.enterFd = function(ctx) {
};

// Exit a parse tree produced by LogoParser#fd.
LogoListener.prototype.exitFd = function(ctx) {
};


// Enter a parse tree produced by LogoParser#bk.
LogoListener.prototype.enterBk = function(ctx) {
};

// Exit a parse tree produced by LogoParser#bk.
LogoListener.prototype.exitBk = function(ctx) {
};


// Enter a parse tree produced by LogoParser#rt.
LogoListener.prototype.enterRt = function(ctx) {
};

// Exit a parse tree produced by LogoParser#rt.
LogoListener.prototype.exitRt = function(ctx) {
};


// Enter a parse tree produced by LogoParser#lt.
LogoListener.prototype.enterLt = function(ctx) {
};

// Exit a parse tree produced by LogoParser#lt.
LogoListener.prototype.exitLt = function(ctx) {
};


// Enter a parse tree produced by LogoParser#cs.
LogoListener.prototype.enterCs = function(ctx) {
};

// Exit a parse tree produced by LogoParser#cs.
LogoListener.prototype.exitCs = function(ctx) {
};


// Enter a parse tree produced by LogoParser#pu.
LogoListener.prototype.enterPu = function(ctx) {
};

// Exit a parse tree produced by LogoParser#pu.
LogoListener.prototype.exitPu = function(ctx) {
};


// Enter a parse tree produced by LogoParser#pd.
LogoListener.prototype.enterPd = function(ctx) {
};

// Exit a parse tree produced by LogoParser#pd.
LogoListener.prototype.exitPd = function(ctx) {
};


// Enter a parse tree produced by LogoParser#ht.
LogoListener.prototype.enterHt = function(ctx) {
};

// Exit a parse tree produced by LogoParser#ht.
LogoListener.prototype.exitHt = function(ctx) {
};


// Enter a parse tree produced by LogoParser#st.
LogoListener.prototype.enterSt = function(ctx) {
};

// Exit a parse tree produced by LogoParser#st.
LogoListener.prototype.exitSt = function(ctx) {
};


// Enter a parse tree produced by LogoParser#home.
LogoListener.prototype.enterHome = function(ctx) {
};

// Exit a parse tree produced by LogoParser#home.
LogoListener.prototype.exitHome = function(ctx) {
};


// Enter a parse tree produced by LogoParser#stop.
LogoListener.prototype.enterStop = function(ctx) {
};

// Exit a parse tree produced by LogoParser#stop.
LogoListener.prototype.exitStop = function(ctx) {
};


// Enter a parse tree produced by LogoParser#setxy.
LogoListener.prototype.enterSetxy = function(ctx) {
};

// Exit a parse tree produced by LogoParser#setxy.
LogoListener.prototype.exitSetxy = function(ctx) {
};


// Enter a parse tree produced by LogoParser#random.
LogoListener.prototype.enterRandom = function(ctx) {
};

// Exit a parse tree produced by LogoParser#random.
LogoListener.prototype.exitRandom = function(ctx) {
};


// Enter a parse tree produced by LogoParser#getangle.
LogoListener.prototype.enterGetangle = function(ctx) {
};

// Exit a parse tree produced by LogoParser#getangle.
LogoListener.prototype.exitGetangle = function(ctx) {
};


// Enter a parse tree produced by LogoParser#getx.
LogoListener.prototype.enterGetx = function(ctx) {
};

// Exit a parse tree produced by LogoParser#getx.
LogoListener.prototype.exitGetx = function(ctx) {
};


// Enter a parse tree produced by LogoParser#gety.
LogoListener.prototype.enterGety = function(ctx) {
};

// Exit a parse tree produced by LogoParser#gety.
LogoListener.prototype.exitGety = function(ctx) {
};


// Enter a parse tree produced by LogoParser#repcount.
LogoListener.prototype.enterRepcount = function(ctx) {
};

// Exit a parse tree produced by LogoParser#repcount.
LogoListener.prototype.exitRepcount = function(ctx) {
};


// Enter a parse tree produced by LogoParser#fore.
LogoListener.prototype.enterFore = function(ctx) {
};

// Exit a parse tree produced by LogoParser#fore.
LogoListener.prototype.exitFore = function(ctx) {
};


// Enter a parse tree produced by LogoParser#pc.
LogoListener.prototype.enterPc = function(ctx) {
};

// Exit a parse tree produced by LogoParser#pc.
LogoListener.prototype.exitPc = function(ctx) {
};


// Enter a parse tree produced by LogoParser#cc.
LogoListener.prototype.enterCc = function(ctx) {
};

// Exit a parse tree produced by LogoParser#cc.
LogoListener.prototype.exitCc = function(ctx) {
};


// Enter a parse tree produced by LogoParser#hexcolor.
LogoListener.prototype.enterHexcolor = function(ctx) {
};

// Exit a parse tree produced by LogoParser#hexcolor.
LogoListener.prototype.exitHexcolor = function(ctx) {
};


// Enter a parse tree produced by LogoParser#pause.
LogoListener.prototype.enterPause = function(ctx) {
};

// Exit a parse tree produced by LogoParser#pause.
LogoListener.prototype.exitPause = function(ctx) {
};


// Enter a parse tree produced by LogoParser#ds.
LogoListener.prototype.enterDs = function(ctx) {
};

// Exit a parse tree produced by LogoParser#ds.
LogoListener.prototype.exitDs = function(ctx) {
};


// Enter a parse tree produced by LogoParser#fontname.
LogoListener.prototype.enterFontname = function(ctx) {
};

// Exit a parse tree produced by LogoParser#fontname.
LogoListener.prototype.exitFontname = function(ctx) {
};


// Enter a parse tree produced by LogoParser#fontsize.
LogoListener.prototype.enterFontsize = function(ctx) {
};

// Exit a parse tree produced by LogoParser#fontsize.
LogoListener.prototype.exitFontsize = function(ctx) {
};


// Enter a parse tree produced by LogoParser#fontstyle.
LogoListener.prototype.enterFontstyle = function(ctx) {
};

// Exit a parse tree produced by LogoParser#fontstyle.
LogoListener.prototype.exitFontstyle = function(ctx) {
};


// Enter a parse tree produced by LogoParser#style.
LogoListener.prototype.enterStyle = function(ctx) {
};

// Exit a parse tree produced by LogoParser#style.
LogoListener.prototype.exitStyle = function(ctx) {
};


// Enter a parse tree produced by LogoParser#number.
LogoListener.prototype.enterNumber = function(ctx) {
};

// Exit a parse tree produced by LogoParser#number.
LogoListener.prototype.exitNumber = function(ctx) {
};


// Enter a parse tree produced by LogoParser#comment.
LogoListener.prototype.enterComment = function(ctx) {
};

// Exit a parse tree produced by LogoParser#comment.
LogoListener.prototype.exitComment = function(ctx) {
};



exports.LogoListener = LogoListener;