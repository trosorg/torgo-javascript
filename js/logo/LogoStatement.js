/* 
 * Copyright 2015-2017 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0  = function (the "License"){};
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var codeBlock = require('../torgo/CodeBlock');
var consts = require('../torgo/CodeConstants');
var exp = require('./ExpressionListener');

/**
 * InterpreterLocation Constructor
 *
 * @returns {InterpreterLocation}
 */
function InterpreterLocation(ctxPos1, ctxPos2) {
    this.column = ctxPos1.column;
    //set offset for codemirror
    this.line = ctxPos1.line - 1;
    this.start = ctxPos1.start;
    //assumes that the code will not span lines, which is fine for Logo
    this.stop = ctxPos1.start + (ctxPos2.stop - ctxPos1.start);
}

/**
 * @returns {InterpreterLocation}
 */
InterpreterLocation.prototype.copy = function () {
    var ret = new InterpreterLocation(this);
    return ret;
};

/**
 * TurtleState Constructor
 *
 * @returns {TurtleState}
 */
function TurtleState() {
    this.penColor = "#000000";
    this.fontStyle = "plain";
    this.fontSize = "12";
    this.fontFamily = "Arial";
    this.penup = false;
    this.showTurtle = true;
    this.angle = 0;
    this.penX = 0;
    this.penY = 0;

    this.height = -1;
    this.width = -1;
    return this;
}

/**
 * @returns {TurtleState}
 */
TurtleState.prototype.copy = function () {
    var ret = new TurtleState();
    ret.penColor = this.penColor;
    ret.fontStyle = this.fontStyle;
    ret.fontSize = this.fontSize;
    ret.fontFamily = this.fontFamily;
    ret.penup = this.penup;
    ret.showTurtle = this.showTurtle;
    ret.angle = this.angle;
    ret.penX = this.penX;
    ret.penY = this.penY;

    ret.height = this.height;
    ret.width = this.width;
    return ret;
};

/**
 * @param {TurtleState} cp
 */
TurtleState.prototype.copyFrom = function (cp) {
    this.penColor = cp.penColor;
    this.fontStyle = cp.fontStyle;
    this.fontSize = cp.fontSize;
    this.fontFamily = cp.fontFamily;
    this.penup = cp.penup;
    this.showTurtle = cp.showTurtle;
    this.angle = cp.angle;
    this.penX = cp.penX;
    this.penY = cp.penY;

    this.height = cp.height;
    this.width = cp.width;
};

var turtleState = new TurtleState();
turtleState.callBackMove = function (canvas, turtle1, turtle2) {
    var context = canvas.getContext('2d');

    context.beginPath();
    context.moveTo(turtle1.penX, turtle1.penY);
    context.lineTo(turtle2.penX, turtle2.penY);
    context.lineWidth = 1;

    // set line color
    context.strokeStyle = turtle1.penColor;
    context.stroke();
};
turtleState.callBackPrint = function (cons, str) {
    cons.value += str + "\n";
};
turtleState.callBackDrawString = function (canvas, str, turtle) {
    var context = canvas.getContext('2d');
    context.translate(turtle.penX, turtle.penY);
    context.scale(1, 1);
    context.rotate(turtle.angle);

    context.fillStyle = turtle.penColor;
    context.font = (turtle.fontStyle === "plain" ? "" : turtle.fontStyle + " ") + turtle.fontSize + "px " + turtle.fontFamily;
    context.fillText(str, 0, 0);
    context.setTransform(1, 0, 0, 1, 0, 0);
};
turtleState.callBackClear = function (canvas) {
    var context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
};
turtleState.callBackCanvasColor = function (canvas, color) {
    canvas.style.backgroundColor = color;
};

// inherit default listener
LogoStatement.prototype = Object.create(codeBlock.CodeBlock.prototype);
LogoStatement.prototype.constructor = LogoStatement;


/**
 * LogoStatement
 *
 * @param {string} command
 * @param ctx
 * @returns {ExpressionListener}
 */
function LogoStatement(command, ctx) {
    this.command = command;
    codeBlock.CodeBlock.call(this, ctx);
    return this;
}
;

turtleState.getWidth = function () {
    var canvas = document.getElementById("canvas");
    return canvas !== null ? canvas.width : 0;
};

turtleState.getHeight = function () {
    var canvas = document.getElementById("canvas");
    return canvas !== null ? canvas.height : 0;
};

function colorToRGBA(color) {
    // Returns the color as an array of [r, g, b, a] -- all range from 0 - 255
    // color must be a valid canvas fillStyle. This will cover most anything
    // you'd want to use.
    // Examples:
    // colorToRGBA('red')  # [255, 0, 0, 255]
    // colorToRGBA('#f00') # [255, 0, 0, 255]
    var cvs, ctx;
    cvs = document.createElement('canvas');
    cvs.height = 1;
    cvs.width = 1;
    ctx = cvs.getContext('2d');
    ctx.fillStyle = color;
    ctx.fillRect(0, 0, 1, 1);
    return ctx.getImageData(0, 0, 1, 1).data;
}

function byteToHex(num) {
    // Turns a number (0-255) into a 2-character hex number (00-ff)
    return ('0' + num.toString(16)).slice(-2);
}

function colourNameToHex(colour)
{
    var colours = {"aliceblue": "#f0f8ff", "antiquewhite": "#faebd7", "aqua": "#00ffff", "aquamarine": "#7fffd4", "azure": "#f0ffff",
        "beige": "#f5f5dc", "bisque": "#ffe4c4", "black": "#000000", "blanchedalmond": "#ffebcd", "blue": "#0000ff", "blueviolet": "#8a2be2", "brown": "#a52a2a", "burlywood": "#deb887",
        "cadetblue": "#5f9ea0", "chartreuse": "#7fff00", "chocolate": "#d2691e", "coral": "#ff7f50", "cornflowerblue": "#6495ed", "cornsilk": "#fff8dc", "crimson": "#dc143c", "cyan": "#00ffff",
        "darkblue": "#00008b", "darkcyan": "#008b8b", "darkgoldenrod": "#b8860b", "darkgray": "#a9a9a9", "darkgreen": "#006400", "darkkhaki": "#bdb76b", "darkmagenta": "#8b008b", "darkolivegreen": "#556b2f",
        "darkorange": "#ff8c00", "darkorchid": "#9932cc", "darkred": "#8b0000", "darksalmon": "#e9967a", "darkseagreen": "#8fbc8f", "darkslateblue": "#483d8b", "darkslategray": "#2f4f4f", "darkturquoise": "#00ced1",
        "darkviolet": "#9400d3", "deeppink": "#ff1493", "deepskyblue": "#00bfff", "dimgray": "#696969", "dodgerblue": "#1e90ff",
        "firebrick": "#b22222", "floralwhite": "#fffaf0", "forestgreen": "#228b22", "fuchsia": "#ff00ff",
        "gainsboro": "#dcdcdc", "ghostwhite": "#f8f8ff", "gold": "#ffd700", "goldenrod": "#daa520", "gray": "#808080", "green": "#008000", "greenyellow": "#adff2f",
        "honeydew": "#f0fff0", "hotpink": "#ff69b4",
        "indianred ": "#cd5c5c", "indigo": "#4b0082", "ivory": "#fffff0", "khaki": "#f0e68c",
        "lavender": "#e6e6fa", "lavenderblush": "#fff0f5", "lawngreen": "#7cfc00", "lemonchiffon": "#fffacd", "lightblue": "#add8e6", "lightcoral": "#f08080", "lightcyan": "#e0ffff", "lightgoldenrodyellow": "#fafad2",
        "lightgrey": "#d3d3d3", "lightgreen": "#90ee90", "lightpink": "#ffb6c1", "lightsalmon": "#ffa07a", "lightseagreen": "#20b2aa", "lightskyblue": "#87cefa", "lightslategray": "#778899", "lightsteelblue": "#b0c4de",
        "lightyellow": "#ffffe0", "lime": "#00ff00", "limegreen": "#32cd32", "linen": "#faf0e6",
        "magenta": "#ff00ff", "maroon": "#800000", "mediumaquamarine": "#66cdaa", "mediumblue": "#0000cd", "mediumorchid": "#ba55d3", "mediumpurple": "#9370d8", "mediumseagreen": "#3cb371", "mediumslateblue": "#7b68ee",
        "mediumspringgreen": "#00fa9a", "mediumturquoise": "#48d1cc", "mediumvioletred": "#c71585", "midnightblue": "#191970", "mintcream": "#f5fffa", "mistyrose": "#ffe4e1", "moccasin": "#ffe4b5",
        "navajowhite": "#ffdead", "navy": "#000080",
        "oldlace": "#fdf5e6", "olive": "#808000", "olivedrab": "#6b8e23", "orange": "#ffa500", "orangered": "#ff4500", "orchid": "#da70d6",
        "palegoldenrod": "#eee8aa", "palegreen": "#98fb98", "paleturquoise": "#afeeee", "palevioletred": "#d87093", "papayawhip": "#ffefd5", "peachpuff": "#ffdab9", "peru": "#cd853f", "pink": "#ffc0cb", "plum": "#dda0dd", "powderblue": "#b0e0e6", "purple": "#800080",
        "rebeccapurple": "#663399", "red": "#ff0000", "rosybrown": "#bc8f8f", "royalblue": "#4169e1",
        "saddlebrown": "#8b4513", "salmon": "#fa8072", "sandybrown": "#f4a460", "seagreen": "#2e8b57", "seashell": "#fff5ee", "sienna": "#a0522d", "silver": "#c0c0c0", "skyblue": "#87ceeb", "slateblue": "#6a5acd", "slategray": "#708090", "snow": "#fffafa", "springgreen": "#00ff7f", "steelblue": "#4682b4",
        "tan": "#d2b48c", "teal": "#008080", "thistle": "#d8bfd8", "tomato": "#ff6347", "turquoise": "#40e0d0",
        "violet": "#ee82ee",
        "wheat": "#f5deb3", "white": "#ffffff", "whitesmoke": "#f5f5f5",
        "yellow": "#ffff00", "yellowgreen": "#9acd32"};

    if (typeof colours[colour.toLowerCase()] !== 'undefined')
        return colours[colour.toLowerCase()];

    return colour;
}

function colorToHex(color) {
    if (typeof (document) === 'undefined') {
        return colourNameToHex(color);
    }
    // Convert any CSS color to a hex representation
    // Examples:
    // colorToHex('red')            # '#ff0000'
    // colorToHex('rgb(255, 0, 0)') # '#ff0000'
    var rgba, hex;
    rgba = colorToRGBA(color);
    hex = [0, 1, 2].map(
            function (idx) {
                return byteToHex(rgba[idx]);
            }
    ).join('');
    return "#" + hex;
}

// inherit default listener
LogoStatement.prototype = Object.create(codeBlock.CodeBlock.prototype);
LogoStatement.prototype.constructor = LogoStatement;

LogoStatement.prototype.process = function (scope) {
    scope.setGlobal(consts.TURTLE_X_VAR, turtleState.penX);
    scope.setGlobal(consts.TURTLE_Y_VAR, turtleState.penY);
    scope.setGlobal(consts.TURTLE_ANGLE_VAR, turtleState.angle);
    var evaluator = new exp.ExpressionListener(scope);
    var ctx = codeBlock.CodeBlock.prototype.getParserRuleContext.call(this);
    var ret = consts.SUCCESS;
    var elem = null;
    var canvas = null;
    if (typeof (document) !== 'undefined') {
        elem = document.getElementById("console");
        canvas = document.getElementById("canvas");
    }

    var ctxPos = new InterpreterLocation(ctx.start, ctx.stop);

    turtleState.callBackInterpreterListener(ctxPos);

    switch (this.command) {
        case "cs":
            turtleState.callBackClear(canvas);

            break;
        case "pu":
            turtleState.penup = true;
            break;
        case "pd":
            turtleState.penup = false;
            break;
        case "fd":
            var distance = evaluator.evalCtx(ctx.expression());
            var newx = turtleState.penX + (distance * Math.cos(turtleState.angle));
            var newy = turtleState.penY + (distance * Math.sin(turtleState.angle));

            var cp1 = turtleState.copy();

            turtleState.penX = newx;
            turtleState.penY = newy;

            if (!turtleState.penup) {
                turtleState.callBackMove(canvas, cp1, turtleState.copy());
            }
            break;
        case "bk":
            var distance = evaluator.evalCtx(ctx.expression());
            var newx = turtleState.penX - (distance * Math.cos(turtleState.angle));
            var newy = turtleState.penY - (distance * Math.sin(turtleState.angle));

            var cp1 = turtleState.copy();

            turtleState.penX = newx;
            turtleState.penY = newy;

            if (!turtleState.penup) {
                turtleState.callBackMove(canvas, cp1, turtleState.copy());
            }
            break;
        case "rt":
            var angle = evaluator.evalCtx(ctx.expression());
            turtleState.angle += Math.PI * angle / 180.0;
            break;
        case "lt":
            var angle = evaluator.evalCtx(ctx.expression());
            turtleState.angle -= Math.PI * angle / 180.0;
            break;
        case "ht":
            turtleState.showTurtle = false;
            break;
        case "st":
            turtleState.showTurtle = true;
            break;
        case "pause":
            console.log("LogoStatement: pause");
            break;
        case "stop":
            ret = consts.RETURN;
            break;
        case "localmake":
            var name = ctx.getChild(1).getText().substring(1);
            var value = evaluator.evalCtx(ctx.getChild(2));
            scope.setNew(name, value);
            break;
        case "make":
            var name = ctx.getChild(1).getText().substring(1);
            var value = evaluator.evalCtx(ctx.getChild(2));
            scope.set(name, value);
            break;
        case "setxy":
            var x = evaluator.evalCtx(ctx.expression(0));
            var y = evaluator.evalCtx(ctx.expression(1));

            var x2 = (turtleState.width > 0 ? turtleState.width : turtleState.getWidth()) / 2.0 + x;
            var y2 = (turtleState.height > 0 ? turtleState.height : turtleState.getHeight()) / 2.0 + y;

            var cp1 = turtleState.copy();

            turtleState.penX = x2;
            turtleState.penY = y2;

            if (!turtleState.penup) {
                turtleState.callBackMove(canvas, cp1, turtleState.copy());
            }
            break;
        case "home":
            turtleState.penX = turtleState.width > 0 ? turtleState.width / 2.0 : turtleState.getWidth() / 2.0;
            turtleState.penY = turtleState.height > 0 ? turtleState.height / 2.0 : turtleState.getHeight() / 2.0;
            turtleState.angle = -1.0 * (Math.PI / 2.0);

            break;
        case "print":
            var value = evaluator.evalCtx(ctx.getChild(1));
            turtleState.callBackPrint(elem, value);
            console.log(value);
            break;
        case "fontsize":
            var value = evaluator.evalCtx(ctx.getChild(1));
            turtleState.fontSize = value;
            break;
        case "fontstyle":
            var styleString = ctx.style().getText();
            if (null !== styleString) {
                switch (styleString) {
                    case "bold":
                    case "italic":
                        turtleState.fontStyle = styleString;
                        break;
                    case "bold_italic":
                        turtleState.fontStyle = "bold italic";
                        break;
                    case "plain":
                    default:
                        turtleState.fontStyle = "plain";
                        break;
                }
            }
            break;
        case "fontname":
            var name = ctx.name().STRING().getText();
            turtleState.fontFamily = name;
            break;
        case "pc":
            if (ctx.expression().length >= 3) {
                var r = evaluator.evalCtx(ctx.expression(0));
                var g = evaluator.evalCtx(ctx.expression(1));
                var b = evaluator.evalCtx(ctx.expression(2));
                turtleState.penColor = "#" + byteToHex(r) + byteToHex(g) + byteToHex(b);
            } else if (ctx.hexcolor() !== null) {
                var color = ctx.hexcolor().HEX().getText();
                turtleState.penColor = "#" + color;
            } else {
                var name = ctx.name().STRING().getText();
                turtleState.penColor = colorToHex(name);
            }
            break;
        case "cc":
            var color;
            if (ctx.expression().length >= 3) {
                var r = evaluator.evalCtx(ctx.expression(0));
                var g = evaluator.evalCtx(ctx.expression(1));
                var b = evaluator.evalCtx(ctx.expression(2));
                color = "#" + byteToHex(r) + byteToHex(g) + byteToHex(b);
            } else if (ctx.hexcolor() !== null) {
                var color = ctx.hexcolor().HEX().getText();
                color = "#" + color;
            } else {
                var name = ctx.name().STRING().getText();
                var canvasColor = colorToHex(name);
                color = canvasColor;
            }
            turtleState.callBackCanvasColor(canvas, color);

            break;
        case "ds":
            var str = null;
            if (ctx.value().STRINGLITERAL() !== null) {
                str = ctx.value().STRINGLITERAL().getText().substring(1);
            } else if (ctx.value().deref() !== null) {
//                LOGGER.info("ds()");
                //this doesn't seem to be called during dref, instead it is
                //evaluated as an expression...
//                    String n = fd.value().deref().name().STRING().toString();
//                    str = Double.toString(scope.get(n));
            } else if (ctx.value().expression() !== null) {
                str = evaluator.evalCtx(ctx);
            }

            turtleState.callBackDrawString(canvas, str, turtleState.copy());
            break;
        default:
            var fn = codeBlock.CodeBlock.prototype.getFunction.call(this, this.command, scope);
            if (fn !== null && typeof (fn) !== 'undefined') {
                var paramNames = [];
                var paramValues = {};

                var funct = fn.getParserRuleContext();
                var params = funct.parameterDeclarations();
                for (var ii = 0; ii < params.length; ii++) {
                    paramNames.push(params[ii].getText().substring(1));
                }
                for (var ii = 0; ii < paramNames.length; ii++) {
                    var val = evaluator.evalCtx(ctx.expression(ii));
                    paramValues[paramNames[ii]] = val;
                }

                //Invoke the procedure w/ the parameters
                ret = fn.process(scope, paramValues);
            } else {
                ret = consts.HALT;
                console.log("No function " + this.command + " found!");
            }
    }
    return ret;
};

exports.LogoStatement = LogoStatement;
exports.TurtleState = turtleState;
