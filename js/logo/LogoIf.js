/* 
 * Copyright 2015-2017 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0  = function (the "License"){};
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var codeBlock = require('../torgo/CodeBlock');
var exp = require('./ExpressionListener');
var consts = require('../torgo/CodeConstants');

/**
 * LogoIf
 *
 * @param ctx
 * @returns {LogoIf}
 */
function LogoIf(ctx) {
    codeBlock.CodeBlock.call(this, ctx);
    return this;
}

// inherit default listener
LogoIf.prototype = Object.create(codeBlock.CodeBlock.prototype);
LogoIf.prototype.constructor = LogoIf;

LogoIf.prototype.process = function (scope) {
    scope.push(this);

    var evaluator = new exp.ExpressionListener(scope);
    var ct = codeBlock.CodeBlock.prototype.getParserRuleContext.call(this);
    var comparator = ct.comparison().comparisonOperator().getText();
    var val2 = Number(evaluator.evalCtx(ct.comparison().expression(1)));
    var val1 = Number(evaluator.evalCtx(ct.comparison().expression(0)));
    var ret = consts.SUCCESS;

    switch (comparator) {
        case ">":
            if (val1 > val2) {
                ret = codeBlock.CodeBlock.prototype.process.call(this, scope);
            }
            break;
        case "<":
            if (val1 < val2) {
                ret = codeBlock.CodeBlock.prototype.process.call(this, scope);
            }
            break;
        case ">=":
            if (val1 >= val2) {
                ret = codeBlock.CodeBlock.prototype.process.call(this, scope);
            }
            break;
        case "<=":
            if (val1 <= val2) {
                ret = codeBlock.CodeBlock.prototype.process.call(this, scope);
            }
            break;
        case "=":
        case "==":
            if (val1 === val2) {
                ret = codeBlock.CodeBlock.prototype.process.call(this, scope);
            }
            break;
        case "<>":
        case "!=":
        case "!":
            if (val1 !== val2) {
                ret = codeBlock.CodeBlock.prototype.process.call(this, scope);
            }
            break;
        default:
            break;
    }

    scope.pop();
    return ret;
};

exports.LogoIf = LogoIf;

