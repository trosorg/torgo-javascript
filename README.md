# torgo-javascript

Web-App version of Torgo utilizing JavaScript

This currently uses the 'requires' library which uses the XMLHttpRequest object to load the javascript files.  
This means that the web-app must be hosted on a server and cannot run (as is) standalone.

See it live at [tros.org](http://tros.org/torgo-javascript/).