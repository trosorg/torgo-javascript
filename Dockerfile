#
# Nginx Dockerfile
#
# https://github.com/dockerfile/nginx
#

# Pull base image.
FROM ubuntu:18.04

# Install.
RUN \
  sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list && \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y software-properties-common && \
  add-apt-repository -y ppa:nginx/stable && \
  apt-get update && \
  apt-get install -y nginx && \
  mkdir -p /var/www/html/ && \
  rm -rf /var/lib/apt/lists/* && \
  mkdir -p /opt/torgo/bin && \
  echo "\ndaemon off;" >> /etc/nginx/nginx.conf && \
  chown -R www-data:www-data /var/lib/nginx

ADD antlr4 /var/www/html/antlr4
ADD css /var/www/html/css
ADD data /var/www/html/data
ADD img /var/www/html/img
ADD js /var/www/html/js
ADD lib /var/www/html/lib

COPY docker-entrypoint.sh /opt/torgo/bin/docker-entrypoint.sh
COPY torgo.conf /etc/nginx/sites-available/default
COPY favicon.ico /var/www/html/favicon.ico
COPY index.html /var/www/html/index.html
COPY logoWorker.js /var/www/html/logoWorker.js

# Define mountable directories.
VOLUME ["/var/log/nginx", "/var/www/html/data"]

# Define working directory.
WORKDIR /etc/nginx

# Define default command.
CMD ["/opt/torgo/bin/docker-entrypoint.sh"]

# Expose ports.
EXPOSE 80
