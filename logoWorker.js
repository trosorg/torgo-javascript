/* 
 * Copyright 2015-2017 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
importScripts('lib/require.js');
var antlr4 = require('antlr4/index');
var interpreter = require('js/index');
var logo = interpreter.logo;
var torgo = interpreter.torgo;

addEventListener('message', function (e) {
    var input = e.data[0];
    var width = e.data[1];
    var height = e.data[2];
    var useDynamicScope = e.data[3];
    var chars = new antlr4.InputStream(input);
    var lexer = new logo.antlr.LogoLexer.LogoLexer(chars);
    var tokens = new antlr4.CommonTokenStream(lexer);
    var parser = new logo.antlr.LogoParser.LogoParser(tokens);
    parser.buildParseTrees = true;
    var tree = parser.prog();
    var printer = new logo.LexicalListener.LexicalListener();
    logo.TurtleState.getWidth = function () {
        return width;
    };
    logo.TurtleState.getHeight = function () {
        return height;
    };
    logo.TurtleState.callBackMove = function (canvas, turtleState1, turtleState2) {
        postMessage(["move", turtleState1, turtleState2]);
    };
    logo.TurtleState.callBackInterpreterListener = function (ctxPos) {
        postMessage(["interpreter listener", ctxPos]);
    };
    logo.TurtleState.callBackPrint = function (cons, val) {
        postMessage(["print", val]);
    };
    logo.TurtleState.callBackClear = function (canvas) {
        postMessage(["clear"]);
    };
    logo.TurtleState.callBackCanvasColor = function (canvas, color) {
        postMessage(["canvas", color]);
    };
    logo.TurtleState.callBackDrawString = function (canvas, str, turtle) {
        postMessage(["drawstring", str, turtle]);
    };

    antlr4.tree.ParseTreeWalker.DEFAULT.walk(printer, tree);
    var entry = printer.getEntryPoint();
    var scope = new torgo.DynamicScope.DynamicScope();
    if (!useDynamicScope) {
        scope = new torgo.LexicalScope.LexicalScope();
    }
    entry.process(scope);

    postMessage(["done"]);
    close();
}, false);